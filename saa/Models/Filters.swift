//
//  Filters.swift
//  saa
//
//

import Foundation

struct BarDataInfo: Hashable {
    let date: Date
    let value: Int
}


enum Filters: Int, CaseIterable {
    case live = 0
    case today = 1
    case yesterday = 3
    case thisWeek = 5
    case thisMonth = 7
    
    var title: String {
        switch self {
        case .live:
            return "Live"
        case .today:
            return "Today"
        case .yesterday:
            return "Yesterday"
        case .thisWeek:
            return "This week"
        case .thisMonth:
            return "This month"
        }
    }
    
    var filterDates: [Date] {
        switch self {
        case .live, .today:
            return [Date()]
        case .yesterday:
            return [Date.yesterday]
        case .thisWeek:
            return [Date().mondayOfWeek(),  Date()]
        case .thisMonth:
            return [Date().startOfMonth(), Date()]
        }
    }
    
    var buttonTitle: String {
        if case .live = self {
            return "See Live View"
        }
        return "View dashboard"
    }
}

enum FilterChartSection: Int,  CaseIterable {
    case live = 0
    case todaySales
    case todaySessions
    case yesterdaySales
    case yesterdaySessions
    case weekSales
    case weekSessions
    case monthSales
    case monthSessions
}

enum BarDataType: Equatable, Hashable {
    case live(visitors: Int)
    case todaySales(total: Double, orders: Int)
    case todaySessions(sessions: Int, visitors: Int)
    case yesterdaySales(total: Double, orders: Int)
    case yesterdaySessions(sessions: Int, visitors: Int)
    case weekSales(total: Double, orders: Int)
    case weekSessions(sessions: Int, visitors: Int)
    case monthSales(total: Double, orders: Int)
    case monthSessions(sessions: Int, visitors: Int)
    
    var dateFormat: String {
        switch self {
        case .live, .todaySales, .todaySessions:
            return "h a"
        case .yesterdaySales, .yesterdaySessions:
            return "h a"
        case .weekSales, .weekSessions:
            return "E"
        case .monthSales, .monthSessions:
            return "d"
        }
    }
    
    var xAxisValues: [Date] {
        return generateDates()
    }
    
    func generateDates() -> [Date] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        var dates = [Date]()
        let calendar = Calendar.current
        let now = Date()
        let startOfDay = calendar.startOfDay(for: now)
        
        let times = ["12 AM", "4 AM", "8 AM"]
        for time in times {
            let dateString = "\(startOfDay) \(time)"
            if let date = dateFormatter.date(from: dateString) {
                if date >= now {
                    dates.append(date)
                } else {
                    let tomorrow = calendar.date(byAdding: .day, value: 1, to: startOfDay)!
                    let tomorrowDateString = "\(tomorrow) \(time)"
                    if let tomorrowDate = dateFormatter.date(from: tomorrowDateString) {
                        dates.append(tomorrowDate)
                    }
                }
            }
        }
        
        return dates
    }

    
    var mainValue: String {
        switch self {
        case .live(let visitors):
            return "\(visitors)"
        case .todaySessions(let sessions, _), .yesterdaySessions(let sessions, _), .weekSessions(let sessions, _), .monthSessions(let sessions, _):
            return "\(sessions.KFormatted)"
        case .todaySales(let total, _), .yesterdaySales(let total, _), .weekSales(let total, _), .monthSales(let total, _):
            let reducedZeros = total.stipeZerosWithKFormat
            return "$\(reducedZeros)"
        }
    }
    
    var mainTitle: String {
        switch self {
        case .live:
            return "Visitors"
        case .todaySessions, .yesterdaySessions, .weekSessions, .monthSessions:
            return "Sessions"
        case .todaySales, .yesterdaySales, .weekSales, .monthSales:
            return "Total Sales"
        }
    }
    
    var secondaryTitle: String {
        switch self {
        case .live:
            return "right now"
        case .todaySessions(_, let visitors), .yesterdaySessions(_, let visitors), .weekSessions(_, let visitors), .monthSessions(_, let visitors):
            return "\(visitors.KFormatted) visitors"
        case .todaySales(_, let orders), .yesterdaySales(_, let orders), .weekSales(_, let orders), .monthSales(_, let orders):
            return "\(orders.KFormatted) orders"
        }
    }
}

struct ChartBarData: Equatable, Hashable {
    static func == (lhs: ChartBarData, rhs: ChartBarData) -> Bool {
        lhs.type == rhs.type
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
    }
    
    let type: BarDataType
    let isPlaceholder: Bool
    let barInfo: [BarDataInfo]

    init(type: BarDataType, minValue: Int = 2, itemsCount: Int = 10, isPlaceholder: Bool = true) {
        self.type = type
        self.isPlaceholder = isPlaceholder
       // let date = Date()
        var datesValues = [Date]()
        switch type {
        case .live(_): datesValues = Date().hoursOfDay()
        case .todaySessions(_, _), .todaySales(_, _): datesValues = Date().hoursOfDay()
        case .yesterdaySessions(_, _), .yesterdaySales(_, _): datesValues = Date.yesterdayHours()
        case .weekSessions(_, _), .weekSales(_, _):
            var resultDates = [Date]()
    
            Date().currentWeekDates().forEach { date in
                let (twelveAM, sixAM, twelvePM, sixPM) = date.splitOnFour()
                resultDates.append(contentsOf: [twelveAM, sixAM, twelvePM, sixPM])
            }
            datesValues = resultDates
        case .monthSessions(_, _), .monthSales(_, _):
            datesValues = Date().monthArray()
        }

        var barInfoArray = [BarDataInfo]()
        
        datesValues.forEach { date in
            let value = BarDataInfo(date: date, value: minValue)
            barInfoArray.append(value)
        }

        self.barInfo = barInfoArray
    }

    init(saleDays: [SaleDay], type: FilterChartSection, minItemCost: Double) {
        self.isPlaceholder = false
        switch type {
        case .weekSessions:
            let weekDays = saleDays.filter({ !$0.date.isInFuture })
            let sessions = weekDays.map({ Int($0.sessions) })
            let visitors = weekDays.map({ $0.visitors })
            let sum = sessions.reduce(0, +)
           
            self.type = .weekSessions(sessions: sum, visitors: visitors.reduce(0, +))
            
            var resultDates = [Date]()
            
            let datesValues = Date().currentWeekDates()

            datesValues.forEach { date in
                let (twelveAM, sixAM, twelvePM, sixPM) = date.splitOnFour()
                resultDates.append(contentsOf: [twelveAM, sixAM, twelvePM, sixPM])
            }
            
            var resultArray = [Int]()
            
            sessions.forEach { value in
                let valueArray = value.splitInteger(1)
                //[value].pad(to: 4, with: placeholder)
                resultArray.append(contentsOf: valueArray)
            }
            
            let placeholder = (resultArray.max() ?? sum) * 3 / 100
            let paddedValues = resultArray.pad(to: 28, with: placeholder)

            self.barInfo = zip(resultDates, paddedValues).map({ date, int in
                let value = int < placeholder ? (placeholder + int) : int
                return BarDataInfo(date: date, value: date.isiInFutureTime ? placeholder : value)
            })
        case .weekSales:
            let weekDays = saleDays.filter({ !$0.date.isInFuture })
            let totalSales = weekDays.map({ $0.totalSales(minItemCost) })
            let totalOrders = weekDays.map({ $0.totalOrders })
            let sum = totalSales.reduce(0, +)
           // let placeholder = (totalSales.max() ?? sum) * 4 / 100

            self.type = .weekSales(total: sum, orders: totalOrders.reduce(0, +))

            var resultDates = [Date]()
            
            let datesValues = Date().currentWeekDates()

            datesValues.forEach { date in
                let (twelveAM, sixAM, twelvePM, sixPM) = date.splitOnFour()
                resultDates.append(contentsOf: [twelveAM, sixAM, twelvePM, sixPM])
            }

            var resultArray = [Double]()
            
            totalSales.forEach { value in
                let valueArray = Int(value).splitInteger(Int(minItemCost)).map({ Double($0)})
                resultArray.append(contentsOf: valueArray)
            }
            
            let placeholder = (resultArray.max() ?? sum) * 3 / 100
            let paddedValues = resultArray.pad(to: 28, with: placeholder)

            self.barInfo = zip(resultDates, paddedValues).map({ date, int in
                let value = int < placeholder ? (placeholder + int) : int
                return BarDataInfo(date: date, value: Int(date.isiInFutureTime ? placeholder : value))
            })
        case .monthSessions:
            let month = saleDays.filter({ !$0.date.isInFuture })
            let totalSession = month.map({ $0.sessions })
            let visitors = month.map({ $0.visitors })
            let sum = totalSession.reduce(0, +)
            let placeholder = (totalSession.max() ?? sum) * 3 / 100
            self.type = .monthSessions(sessions: sum, visitors: visitors.reduce(0, +))
            
            let datesValues = Date().monthArray()
            let values = totalSession.pad(to: datesValues.count, with: 10)

            self.barInfo = zip(datesValues, values).map({ date, int in
                BarDataInfo(date: date, value: date.isInFuture ? placeholder : int)
            })
            
        case .monthSales:
            let month = saleDays.filter({ !$0.date.isInFuture })
            let totalSales = month.map({ $0.totalSales(minItemCost) })
            let orders = month.map({ $0.totalOrders})
            let sum = totalSales.reduce(0, +)
            let placeholder = (totalSales.max() ?? sum) * 3 / 100
            self.type = .monthSales(total: sum, orders: orders.reduce(0, +))
            let datesValues = Date().monthArray()
            let values = totalSales.pad(to: Double(datesValues.count), with: 10)

            self.barInfo = zip(datesValues, values).map({ date, int in
                BarDataInfo(date: date, value: Int(date.isInFuture ? placeholder : int))
            })
        default: fatalError("another types should be implemented with other constructor")
        }
    }
    
    init(day: SaleDay, type: FilterChartSection, minItemCost: Double) {
        self.isPlaceholder = false
        switch type {
        case .todaySessions:
            self.type = .todaySessions(sessions: day.sessions, visitors: day.visitors)
            
            let datesValues = Date().hoursOfDay()
            let amountValues = day.generateValue(forSessions: true, minItemCost: minItemCost)
            let placeholder = (amountValues.max() ?? 0) * 3 / 100

            let shuffledValue = amountValues.pad(to: datesValues.filter({ !$0.isiInFutureTime }).count, with: placeholder).shuffled()
            let paddedValues = shuffledValue.pad(to: 24, with: placeholder)
            
            self.barInfo = zip(datesValues, paddedValues).map({ date, int in
                let value = int < placeholder ? (placeholder + int) : int
                return BarDataInfo(date: date, value: date.isiInFutureTime ? placeholder : value)
            })
            
            
        case .todaySales:
            self.type = .todaySales(total: day.totalSales(minItemCost), orders: day.totalOrders)
            let datesValues = Date().hoursOfDay()
            
            let amountValues = day.generateValue(forSessions: false, minItemCost: minItemCost)
            let placeholder = (amountValues.max() ?? 0) * 3 / 100
            
            
            let shuffledValue = amountValues.pad(to: datesValues.filter({ !$0.isiInFutureTime }).count, with: placeholder).shuffled()
            let paddedValues = shuffledValue.pad(to: 24, with: placeholder)
        
 
            let barInfo = zip(datesValues, paddedValues).map({ date, int in
                let value = int < placeholder ? (placeholder + int) : int
                return BarDataInfo(date: date, value: date.isiInFutureTime ? placeholder : value)
            })
            
            self.barInfo = barInfo
            
        case .yesterdaySessions:
            self.type = .yesterdaySessions(sessions: day.sessions, visitors: day.visitors)
            let datesValues = Date().hoursOfDay(isYesterday: true)
            
            let amountValues = day.generateValue(forSessions: true, minItemCost: minItemCost)
            
            let placeholder = (amountValues.max() ?? 0) * 3 / 100
            let paddedValues = amountValues.pad(to: 24, with: placeholder).shuffled()
            self.barInfo = zip(datesValues, paddedValues).map({ date, int in
                let value = int < placeholder ? (placeholder + int) : int
                return BarDataInfo(date: date, value: value)
            })
        case .yesterdaySales:
            self.type = .yesterdaySales(total: day.totalSales(minItemCost), orders: day.totalOrders)
            let datesValues = Date().hoursOfDay(isYesterday: true)
            
            
            let amountValues = day.generateValue(forSessions: false, minItemCost: minItemCost)
            let placeholder = (amountValues.max() ?? 0) * 3 / 100
            
            let paddedValues = amountValues.pad(to: 24, with: placeholder).shuffled()
            self.barInfo = zip(datesValues, paddedValues).map({ date, int in
                let value = int < placeholder ? (placeholder + int) : int
                return BarDataInfo(date: date, value: value)
            })
            
        default:
            fatalError("Another types should be created with other constructor")
        }
    }
    
}
