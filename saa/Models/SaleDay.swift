//
//  SaleDay.swift
//  saa
//
//

import Foundation

struct CurrentUser: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case currentUserId
        case chargebacks
        case orders
        case showChargebacks
        case showOrders
        case leftBehind
        case notificationInterval
        case notificationOrderFor
        case notificationStoreName
        case notificationTotal
        case minItemCost
    }
    
    let uuid: UUID
    let currentUserId: Int
    let chargebacks: Int
    let orders: Int
    let showChargebacks: Bool
    let showOrders: Bool
    let leftBehind: String
    let notificationInterval: Int
    let notificationOrderFor: String
    let notificationStoreName: String
    let notificationTotal: String
    let minItemCost: Double
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.uuid = UUID()
        self.currentUserId = try values.decodeIfPresent(Int.self, forKey: .currentUserId) ?? 1
        self.chargebacks = try values.decodeIfPresent(Int.self, forKey: .chargebacks) ?? 0
        self.orders = try values.decodeIfPresent(Int.self, forKey: .orders) ?? 0
        self.showChargebacks = try values.decodeIfPresent(Bool.self, forKey: .showChargebacks) ?? false
        self.showOrders = try values.decodeIfPresent(Bool.self, forKey: .showOrders) ?? false
        self.leftBehind = try values.decodeIfPresent(String.self, forKey: .leftBehind) ?? ""
        self.notificationInterval = try values.decodeIfPresent(Int.self, forKey: .notificationInterval) ?? 90
        self.notificationOrderFor = try values.decodeIfPresent(String.self, forKey: .notificationOrderFor) ?? ""
        self.notificationStoreName = try values.decodeIfPresent(String.self, forKey: .notificationStoreName) ?? ""
        self.notificationTotal = try values.decodeIfPresent(String.self, forKey: .notificationTotal) ?? ""
        self.minItemCost = try values.decodeIfPresent(Double.self, forKey: .minItemCost) ?? 89.99
    }
    
}

struct SectionValues {
    let main: Double
    let additional: Int
}

enum SaleDayType: Equatable {
    case day
    case range([SaleDay])
}

struct LinearChartData: Hashable, Identifiable, Comparable {
    static func < (lhs: LinearChartData, rhs: LinearChartData) -> Bool {
        return lhs.amount < rhs.amount
    }
    
    var id = UUID().uuidString
    let amount: Int
    let date: Date
    var animate = false
}

struct SaleDay: Codable, Hashable {
    static func == (lhs: SaleDay, rhs: SaleDay) -> Bool {
        lhs.dateString == rhs.dateString && lhs.totalOrders == rhs.totalOrders
    }

    func hash(into hasher: inout Hasher) {
        return hasher.combine(dateString)
    }

    enum CodingKeys: String, CodingKey {
        case addedToCart
        case dateString = "date"
        case reachedCheckout
        case sessionConverted
        case sessions
        case totalOrders
        case visitors
    }

    let addedToCart: Int
    private let dateString: String
    let reachedCheckout: Int
    let sessionConverted: Int
    let sessions: Int
    let totalOrders: Int
    let visitors: Int
    let type: SaleDayType
    var chartData: [LinearChartData]
    
    var monthData: Bool {
        let dates = chartData.map { $0.date }
        return (28...31).contains(dates.count)
    }
    
    var strideBy: Int {
        guard case .range(let array) = type else {
             return 0
        }

        let dates = array.map { $0.date }

        guard dates.count >= 4 else {
            return 0
        }

        let divides = Double(dates.count) / 3.5

        return Int(divides)
    }
    
    
    var animate = false

    var date: Date {
        self.dateString.asDate() ?? Date()
    }
    
    func totalSales(_ value: Double) -> Double {
        return value * Double(totalOrders)
    }

    var conversionRate: Double {
        guard totalOrders != 0 && visitors != 0 else { return 0 }
        let values = Double(totalOrders) / Double(visitors) * 100
        return values
    }
    
    var addedToCartPersentege: Double {
        guard addedToCart != 0 && sessions != 0 else { return 0 }
        return Double(addedToCart) / Double(sessions) * 100
    }
    
    var reachedCheckoutPersentege: Double {
        guard reachedCheckout != 0 && sessions != 0 else { return 0 }
        return Double(reachedCheckout) / Double(sessions) * 100
    }
    
    var sessionConvertedPersentege: Double {
        guard sessionConverted != 0 && sessions != 0 else { return 0 }
        return Double(sessionConverted) / Double(sessions) * 100
    }

    func valuesForSection(type: RateItemSectionType) -> SectionValues {
        switch type {
        case .cartAdded:
            return SectionValues(main: addedToCartPersentege, additional: addedToCart)
        case .checkoutReached:
            return SectionValues(main: reachedCheckoutPersentege, additional: reachedCheckout)
        case .convertedSessions:
            return SectionValues(main: sessionConvertedPersentege, additional: sessionConverted)
        }
    }

    init(range: [SaleDay]) {
        self.type = .range(range)
        self.addedToCart = range.map({ $0.addedToCart }).reduce(0, +)
        self.dateString = range.compactMap({ $0.date }).sorted(by: <).first?.asStringF() ?? Date().asStringF()
        self.reachedCheckout = range.map({ $0.reachedCheckout }).reduce(0, +)
        self.sessionConverted = range.map({ $0.sessionConverted }).reduce(0, +)
        self.sessions = range.map({ $0.sessions }).reduce(0, +)
        self.totalOrders = range.map({ $0.totalOrders }).reduce(0, +)
        self.visitors = range.map({ $0.visitors }).reduce(0, +)
        
        self.chartData = range.map({ LinearChartData(amount: $0.totalOrders, date: $0.date) })
    }

    init(date: Date, zero: Bool = false) {
        self.type = .day//([])
        self.addedToCart = zero ? 0 : Int.random(in: 1...164)
        self.dateString = date.asStringF()
        self.reachedCheckout = zero ? 0 : Int.random(in: 1...110)
        self.sessionConverted = zero ? 0 : Int.random(in: 0...15)
        self.sessions = zero ? 0 : Int.random(in: 131...1680)
        self.totalOrders = zero ? 0 : Int.random(in: 1...18)
        self.visitors = zero ? 0 : Int.random(in: 67...793)
        self.chartData = []
    }
    
    func generateValue(forSessions: Bool, minItemCost: Double) -> [Int] {
        let amount = forSessions ? Double(self.sessions) : (Double(self.totalOrders) * minItemCost)
        let constant = forSessions ? 1 : minItemCost
        
        guard amount != constant else {
            
            return [Int(amount)]
        }
        
        var amountArray = [Double]()
        var divides = abs(Int(amount / constant))
        
       

        while divides != 0 {
            var randomPiece = Int.random(in: 1...Int(divides))
            
            if randomPiece == Int(divides) && divides > 1 {
                randomPiece = Int(Double(randomPiece) * 0.15)
            } else if randomPiece > (divides / 2) && divides > 1 {
                randomPiece = Int(Double(randomPiece) * 0.25)
            }

            amountArray.append(Double(randomPiece) * constant)
            divides -= randomPiece
        }
        let values = amountArray.map({ Int($0) }).sorted(by: >).prefix(24)

        return Array(values)
    }

    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)

        do {
            self.addedToCart = try values.decode(Int.self, forKey: .addedToCart)
            
            self.reachedCheckout = try values.decode(Int.self, forKey: .reachedCheckout)
            self.sessionConverted = try values.decode(Int.self, forKey: .sessionConverted)
            self.sessions = try values.decode(Int.self, forKey: .sessions)
            self.visitors = try values.decode(Int.self, forKey: .visitors)
            
            
            let totalOrders = try values.decode(Int.self, forKey: .totalOrders)
            self.totalOrders = totalOrders
            let dateString = try values.decode(String.self, forKey: .dateString)
            self.dateString = dateString

            self.type = .day

            let orderValues = totalOrders.generateRandomSliceArray()
            let dates = (dateString.asDate() ?? Date()).hoursOfDay()
            var paddedValues = orderValues.pad(to: dates.count, with: 0)
            paddedValues.shuffle()
            
            let chartData = zip(paddedValues, dates).map {
                LinearChartData(amount: $0, date: $1)
            }

            self.chartData = chartData
        } catch {
            debugPrint(error.localizedDescription)
            fatalError(error.localizedDescription)
        }
   }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.addedToCart, forKey: .addedToCart)
        try container.encode(self.dateString, forKey: .dateString)
        try container.encode(self.reachedCheckout, forKey: .reachedCheckout)
        try container.encode(self.sessionConverted, forKey: .sessionConverted)
        try container.encode(self.sessions, forKey: .sessions)
        try container.encode(self.totalOrders, forKey: .totalOrders)
        try container.encode(self.visitors, forKey: .visitors)
    }
}
