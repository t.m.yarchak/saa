//
//  InfoDAO.swift
//  saa
//
//

import Combine
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift
import UserNotifications

class InfoDAO {
    private var cancellables = Set<AnyCancellable>()
    let store: Firestore

    @Published var currentUser: CurrentUser?
    @Published var datesPreset: [SaleDay] = []
    @Published var chartBarData: [ChartBarData] = GeneralConstants.placeholderBarData

    var datesObserver: ListenerRegistration?
    var timer: Timer?

    init(store: Firestore) {
        self.store = store

        self.addCurrentUserListener()
        self.addSubscribers()
    }
    
    func addNotificationTimer(user: CurrentUser) {
        if timer != nil {
            self.timer?.invalidate()
        }

        guard user.notificationInterval > 0 else {
            return
        }

        let string = "\(user.notificationStoreName) has a new order for \(user.notificationOrderFor) totaling \(user.notificationTotal) from Online Store."

        let progressUpdateTimer = Timer(timeInterval: TimeInterval(user.notificationInterval), repeats: true) { timer in
            UNUserNotificationCenter.sentLocalNotification(body: string)
            
        }

        self.timer = progressUpdateTimer
        RunLoop.main.add(progressUpdateTimer, forMode: .common)
    }
    
    func requestPushNotificationAuthorization(user: CurrentUser) {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.requestAuthorization(options: [.badge, .alert, .sound]) { granted, _ in
            DispatchQueue.main.async {
                if granted {
                    UIApplication.shared.registerForRemoteNotifications()
                    self.addNotificationTimer(user: user)
                }
            }
        }
    }

    func addSubscribers() {
        $currentUser
            .compactMap({ $0 })
            .sink { [weak self] user in
                self?.requestPushNotificationAuthorization(user: user)
            //    self?.addNotificationTimer()
            }
            .store(in: &cancellables)
        
        
        $currentUser
            .removeDuplicates()
            .compactMap({ $0?.currentUserId })
            .sink { [weak self] id in
                self?.datesObserver?.remove()
                self?.datesPreset.removeAll()
                self?.chartBarData = GeneralConstants.placeholderBarData
                self?.addDatePresetsListener(for: id)
            }
            .store(in: &cancellables)
        
        $datesPreset
            .sink { [weak self] salesDays in
                guard let self = self else { return }
                let data = self.createChartData(array: salesDays, minItemCost: self.currentUser?.minItemCost ?? 89.99)
                self.chartBarData = data
            }
            .store(in: &cancellables)
    }
    
    private func createChartData(array: [SaleDay], minItemCost: Double) -> [ChartBarData] {
        guard !array.isEmpty else { return [] }
        
        var barData: [ChartBarData] = []
        
        Filters.allCases.forEach { filter in
            switch filter {
            case .live:
                guard let today = array.first(where: { $0.date.isInToday == true }) else {
                    barData.append(ChartBarData(type: .live(visitors: 0), isPlaceholder: false))
                    return
                }
                let live = ChartBarData(type: .live(visitors: today.sessions / 10), isPlaceholder: false)
                barData.append(live)
            case .today:
                guard let todayDay = array.first(where: { $0.date.isInToday == true } ) else {
                    let todaySales = ChartBarData(type: .todaySales(total: 0, orders: 0), itemsCount: 24, isPlaceholder: false)
                    let todaySessions = ChartBarData(type: .todaySessions(sessions: 0, visitors: 0), minValue: 2, itemsCount: 24, isPlaceholder: false)
                    barData.append(contentsOf: [todaySales, todaySessions])
                    return
                }
                let todaySales = ChartBarData(day: todayDay, type: .todaySales, minItemCost: minItemCost)
                let todaySessions = ChartBarData(day: todayDay, type: .todaySessions, minItemCost: minItemCost)
                barData.append(contentsOf: [todaySales, todaySessions])
            case .yesterday:
                guard let todayDay = array.first(where: { $0.date.isInYesterfay == true }) else  {
                    let todaySales = ChartBarData(type: .yesterdaySales(total: 0, orders: 0), itemsCount: 24, isPlaceholder: false)
                    let todaySessions = ChartBarData(type: .yesterdaySessions(sessions: 0, visitors: 0), itemsCount: 24, isPlaceholder: false)
                    barData.append(contentsOf: [todaySales, todaySessions])
                    return
                }

                let todaySales = ChartBarData(day: todayDay, type: .yesterdaySales, minItemCost: minItemCost)
                let todaySessions = ChartBarData(day: todayDay, type: .yesterdaySessions, minItemCost: minItemCost)
                barData.append(contentsOf: [todaySales, todaySessions])
            case .thisWeek:
                let thisWeek = array.filter({ $0.date.isInSameWeek(as: Date()) })
                let todaySales = ChartBarData(saleDays: thisWeek, type: .weekSales, minItemCost: minItemCost)
                let todaySessions = ChartBarData(saleDays: thisWeek, type: .weekSessions, minItemCost: minItemCost)
                barData.append(contentsOf: [todaySales, todaySessions])

            case .thisMonth:
                let thisMonth = array.filter({ $0.date.isSameMonth(as: Date()) == true })
                let todaySales = ChartBarData(saleDays: thisMonth, type: .monthSales, minItemCost: minItemCost)
                let todaySessions = ChartBarData(saleDays: thisMonth, type: .monthSessions, minItemCost: minItemCost)
    
                barData.append(contentsOf: [todaySales, todaySessions])
            }
        }
        return barData
    }

    private func addCurrentUserListener() {
        store.collection("activeUser").addSnapshotListener { querySnapshot, error in
            if let querySnapshot = querySnapshot {
                let _ = querySnapshot.documents.compactMap { document in
                    let user = try? document.data(as: CurrentUser.self)
                    self.currentUser = user
                    
                }
            }
        }
    }
    
    private func addDatePresetsListener(for user: Int) {
        self.datesObserver = store.collection("users").document("\(user)").addSnapshotListener({ querySnapshot, error in
            if let querySnapshot = querySnapshot {
                guard let preset = querySnapshot.get("dates") else { return }
                do {
                    let data = try JSONSerialization.data(withJSONObject: preset)
                    let dates = try JSONDecoder().decode([SaleDay].self, from: data)
                    self.datesPreset = dates
                } catch {
                    debugPrint(error.localizedDescription)
                }
            }
        })
    }

    func generateDay(with day: Date) {
        guard let currentUser = currentUser else { return }

        var saleDays = self.datesPreset
        saleDays.append(SaleDay(date: day))
        self.updateDatesPreset(dates: saleDays.sorted(by: { $0.date < $1.date }), docNumber: currentUser.currentUserId)
    }
    
    func updateDatesPreset(dates: [SaleDay], docNumber: Int = 1) {
        let encodedValues = dates.compactMap { saleDay -> [String : Any]? in
            var encoded: [String: Any]?
            encoded = try? Firestore.Encoder().encode(saleDay)
            return encoded
        }

        store.collection("users").document("\(docNumber)").updateData(["dates": encodedValues])
    }
}
