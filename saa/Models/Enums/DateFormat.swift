//
//  DateFormat.swift
//  saa
//
//

import Foundation

enum DateFormat: String, CaseIterable {
    case enUSPosixZ = "yyyy-MM-dd'T'HH:mm:ssZ"
    case enUSPosixMilisecondsZ = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case enUSPosix = "yyyy-MM-dd'T'HH:mm:ss"
    case monthDayYearHMSa = "MM/dd/yyyy hh:mm:ss a"
    case dayMonthYearHMSa = "dd/MM/yyyy hh:mm:ss a"
    case monthDayYearHMa = "MM/dd/yyyy hh:mm a"
    case dayMonthYearHM = "dd/MM/yyyy HH:mm"
    case dayMonthYearEU = "dd-MM-yyyy"
    case dayMonthYearUS = "MM-dd-yyyy"
    case yearMonthDay = "yyyy-MM-dd"
    case yearMonthDayHourMinuteSecond = "yyyy-MM-dd HH:mm:ss"
    case dmyShort = "d MMM y"
    case hhmmss = "hh:mm:ss a"
    case hhmmss2 = "hh:mm:ss"
    case mdySlashSeparated = "d/MMM/y"
    case monthDayYear = "MM/dd/yyyy"
    case dayMonthYear = "dd/MM/yyyy"
    case monthDayShort = "MM/dd"
    case monthDayLong = "MMMM DDD"
    case hourMinutea = "HH : mm"
    case hourMinute24Hour = "HH:mm"
    case hourMinute12Hour = "hh:mm a"
    case HHmmssNoPM = "HH:mm:ss"
    case apple = "yyyy-MM-dd HH:mm:ss VV"
    case monthDayHourMinute = "dd-MM HH:mm"
}
