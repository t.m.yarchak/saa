//
//  ChartImageEnum.swift
//  saa
//
//

import Foundation


enum ChartImageEnum: Int {
    case chart1 = 1
    case chart2
    case chart3
    
    var imageName: String {
        switch self {
        case .chart1:
            return "Chart-1"
        case .chart2:
            return "Chart-2"
        case .chart3:
            return "Chart-3"
        }
    }
    
    func updateByOne() -> ChartImageEnum {
        guard self.rawValue < 3 else {
            return .chart1
        }
        return self.rawValue == 1 ? .chart2 : .chart3
    }
}
