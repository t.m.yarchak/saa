//
//  UserNotificationsExtensions.swift
//  saa
//
//

import Foundation

import UserNotifications

extension UNUserNotificationCenter {
    static func sentLocalNotification(identifier: String = UUID().uuidString, body: String, sound: UNNotificationSound = .default, delay: TimeInterval = 0.01) {
        let content = UNMutableNotificationContent()
        content.body = body
        content.sound = sound

        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: delay, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        self.current().add(request)
    }

    static func removeNotificationRequest(identifier: String? = nil) {
        guard let identifier = identifier else {
            self.current().removeAllPendingNotificationRequests()
            return
        }
        self.current().removePendingNotificationRequests(withIdentifiers: [identifier])
    }
}
