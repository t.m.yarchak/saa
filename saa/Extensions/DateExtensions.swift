//
//  DateExtensions.swift
//  saa
//
//

import Foundation

extension Date {
    var isInToday: Bool { Calendar.current.isDateInToday(self) }
    var isInYesterfay: Bool { Calendar.current.isDateInYesterday(self) }
    
    static var yesterday: Date { return Date().dayBefore }

    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }

    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    func isSameDay(as date: Date) -> Bool {
        return self.isEqual(to: date, toGranularity: .day)
    }
    
    func isEqual(to date: Date, toGranularity component: Calendar.Component = .day) -> Bool {
        var calendar = Calendar(identifier: .iso8601)
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        calendar.firstWeekday = 2
        return calendar.isDate(self, equalTo: date, toGranularity: component)
    }
    
    func asStringF() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.monthDayYear.rawValue
        return dateFormatter.string(from: self)
    }
    
    var isInFuture: Bool {
        return self.stripTime > Date()
    }
    
    func toTimeZone(_ timeZone: TimeZone = TimeZone(abbreviation: "UTC")!) -> Date {
        let calendar = Calendar.current
        
        var dateComponents = calendar.dateComponents(in: timeZone, from: self)
        dateComponents.timeZone = timeZone
        
        return calendar.date(bySettingHour: dateComponents.hour ?? 0, minute: dateComponents.minute ?? 0, second: dateComponents.second ?? 0, of: self)!
    }
    
    var isiInFutureTime: Bool {
        return self > Date().toTimeZone(.current)
    }
    
    var stripTime: Date {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: self)
        let date = Calendar.current.date(from: components)
        return date ?? Date()
    }
    
    var dayName: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: Locale.current.identifier)
        dateFormatter.dateFormat = "EEEE"// to get long style
        return dateFormatter.string(from: self)
    }
    
    var asString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = .current
        return dateFormatter.string(from: self)
    }
    
    func getAllDates() -> [Date] {
        let calendar = Calendar.current
        let startDate = calendar.date(from: Calendar.current.dateComponents([.year,.month], from: self))!
        
        let range = calendar.range(of: .day, in: .month, for: startDate)!
        
        return range.compactMap { day -> Date in
            return calendar.date(byAdding: .day, value: day - 1, to: startDate)!
        }
    }
    
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
    
    var month: String {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("MMM")
        return formatter.string(from: self)
    }
    
    var day: String {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("DD")
        return formatter.string(from: self)
    }
    
    var weekDay: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "EEE"
        return dateFormatter.string(from: self)
    }
    
    var asTitleString: String {
        let day = self.get(.day)
        return "\(self.month) \(day), \(self.year)"
    }
    
    var asTitleWithMonthString: String {
        let day = self.get(.day)
        return "\(self.month) \(day), \(self.year)"
    }
    
    func asTitle(plusDate: Date) -> String {
        let firstDay = self.get(.day)
        let secondDay = plusDate.get(.day)
        
        return "\(self.month) \(firstDay)- \(plusDate.month) \(secondDay), \(self.year)"
    }
    
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }
    
    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
    
    
    
    func firstAndLastDatesForLast(_ days: Int) -> (firstDate: Date, lastDate: Date)? {
        let calendar = Calendar.current
        let endDate = self
        guard let startDate = calendar.date(byAdding: .day, value: -days, to: endDate) else {
            return nil
        }
        return (startDate, endDate)
    }
    
    func firstAndLastDatesForLastMonth() -> (firstDate: Date, lastDate: Date)? {
        let calendar = Calendar.current
        let endDate = self
        guard let startDate = calendar.date(byAdding: .month, value: -1, to: endDate),
              let firstDayOfMonth = calendar.date(from: calendar.dateComponents([.year, .month], from: startDate)),
              let lastDayOfMonth = calendar.date(byAdding: .day, value: -1, to: firstDayOfMonth) else {
            return nil
        }
        return (firstDayOfMonth, lastDayOfMonth)
    }
    
    func previousYear() -> (firstDate: Date, lastDate: Date)? {
        let calendar = Calendar.current
        guard let previousYear = calendar.date(byAdding: .year, value: -1, to: self) else {
            return nil
        }
        let components = calendar.dateComponents([.year], from: previousYear)
        guard let year = components.year else {
            return nil
        }
        let firstDateComponents = DateComponents(year: year, month: 1, day: 1)
        guard let firstDate = calendar.date(from: firstDateComponents) else {
            return nil
        }
        let lastDateComponents = DateComponents(year: year, month: 12, day: 31)
        guard let lastDate = calendar.date(from: lastDateComponents) else {
            return nil
        }
        return (firstDate, lastDate)
    }
    
    func firstAndLastDatesForQuarter(_ quarter: Int) -> (firstDate: Date, lastDate: Date)? {
        let calendar = Calendar.current
        guard quarter >= 1 && quarter <= 4 else {
            return nil
        }
        let year = calendar.component(.year, from: self)
        let components = DateComponents(year: year, month: (quarter - 1) * 3 + 1, day: 1)
        guard let firstDayOfQuarter = calendar.date(from: components),
              let lastDayOfQuarter = calendar.date(byAdding: DateComponents(month: 3, day: -1), to: firstDayOfQuarter) else {
            return nil
        }
        return (firstDayOfQuarter, lastDayOfQuarter)
    }
    
    func dateForBFCMYear(_ year: Int) -> Date? {
        let calendar = Calendar.current
        let components = DateComponents(year: year, month: 11, day: 1)
        guard let novemberFirst = calendar.date(from: components),
              let weekday = calendar.dateComponents([.weekday], from: novemberFirst).weekday else {
            return nil
        }
        let daysToAdd = 5 - weekday // 5 is Friday
        guard let bfcmDate = calendar.date(byAdding: DateComponents(day: daysToAdd), to: novemberFirst) else {
            return nil
        }
        return bfcmDate
    }
    
    func mondayOfWeek() -> Date {
        let calendar = Calendar.current
        guard let weekStart = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)),
              let monday = calendar.date(byAdding: .day, value: 2, to: weekStart) else {
            return self
        }
        return monday
    }
    
    func monthToDate(timeZone: TimeZone = .current) -> Date {
        var calendar = Calendar.current
        calendar.timeZone = timeZone
        let components = calendar.dateComponents([.year, .month], from: self)
        guard let startOfMonth = calendar.date(from: components) else {
            return self
        }
        return startOfMonth
    }
    
    func startOfQuarter() -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month], from: calendar.startOfDay(for: self))
        guard let month = components.month, let year = components.year else {
            return self
        }
        let quarter: Int = (month - 1) / 3 + 1
        guard let startOfQuarter = calendar.date(from: DateComponents(year: year, month: 1, quarter: quarter)) else {
            return self
        }
        return startOfQuarter
    }
    
    func startOfYear() -> Date {
        let calendar = Calendar.current
        guard let startOfYear = calendar.date(from: calendar.dateComponents([.year], from: calendar.startOfDay(for: self))) else {
            return self
        }
        return startOfYear
    }
    
    func previousMonth() -> Date? {
        let calendar = Calendar.current
        guard let previousMonth = calendar.date(byAdding: .month, value: -1, to: calendar.startOfDay(for: self)) else {
            return nil
        }
        return previousMonth
    }
    
    func startOfPreviousMonth() -> Date {
        let calendar = Calendar.current
        guard let previousMonth = self.previousMonth(),
              let startOfPreviousMonth = calendar.date(from: calendar.dateComponents([.year, .month], from: previousMonth)) else {
            return self
        }
        return startOfPreviousMonth
    }
    
    func endOfPreviousMonth() -> Date {
        let calendar = Calendar.current
        guard let previousMonth = self.previousMonth(),
              let startOfNextMonth = calendar.date(byAdding: .month, value: 1, to: calendar.startOfDay(for: previousMonth)),
              let endOfPreviousMonth = calendar.date(byAdding: .day, value: -1, to: startOfNextMonth) else {
            return self
        }
        return endOfPreviousMonth
    }
    
    func weekToDate() -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        calendar.firstWeekday = 2
        guard let monday = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else {
            return self
        }
        return monday
    }
    
    func isSameMonth(as otherDate: Date) -> Bool {
        let calendar = Calendar.current
        let year1 = calendar.component(.year, from: self)
        let month1 = calendar.component(.month, from: self)
        let year2 = calendar.component(.year, from: otherDate)
        let month2 = calendar.component(.month, from: otherDate)
        return year1 == year2 && month1 == month2
    }

    func generateRandomDatesArray(desiredAmount: Int) -> [Date] {
        let startDate = self
        let endDate = Calendar.current.date(byAdding: .hour, value: 24, to: startDate)!
        let calendar = Calendar.current

        let sliceDuration = (endDate.timeIntervalSince(startDate) / Double(desiredAmount)).rounded()
        var slices: [Date] = []

        for i in 0..<desiredAmount {
            let startOfDay = calendar.startOfDay(for: startDate)
            let currentSliceStart = startOfDay.addingTimeInterval(sliceDuration * Double(i))
            let currentSliceEnd = startOfDay.addingTimeInterval(sliceDuration * Double(i+1))
            slices.append(currentSliceEnd)
        }
        return slices
    }
    
    func startOfMonth() -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month], from: self)
        return calendar.date(from: components)!
    }
    
    func splitOnFour() -> (Date, Date, Date, Date) {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self)
        let month = calendar.component(.month, from: self)
        let day = calendar.component(.day, from: self)
        
        let twelveAM = calendar.date(from: DateComponents(year: year, month: month, day: day, hour: 0, minute: 0, second: 0))!
        let sixAM = calendar.date(from: DateComponents(year: year, month: month, day: day, hour: 6, minute: 0, second: 0))!
        let twelvePM = calendar.date(from: DateComponents(year: year, month: month, day: day, hour: 12, minute: 0, second: 0))!
        let sixPM = calendar.date(from: DateComponents(year: year, month: month, day: day, hour: 18, minute: 0, second: 0))!
        
        return (twelveAM, sixAM, twelvePM, sixPM)
    }
    
    func currentWeekDates() -> [Date] {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        calendar.firstWeekday = 2 // set Monday as the first day of the week
        var startOfWeek = Date()
        var interval = TimeInterval()
        _ = calendar.dateInterval(of: .weekOfYear, start: &startOfWeek, interval: &interval, for: self)
        let startDate = calendar.date(from: calendar.dateComponents([.year, .month, .day], from: startOfWeek))!
        let endDate = calendar.date(byAdding: .day, value: 6, to: startDate)!
        return (0...6).map { calendar.date(byAdding: .day, value: $0, to: startDate)! }
    }
    
    func monthArray() -> [Date] {
        let calendar = Calendar.current
        let range = calendar.range(of: .day, in: .month, for: self)!
        let days = range.map { day -> Date in
            var dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
            dateComponents.day = day
            return calendar.date(from: dateComponents)!
        }
        return days
    }

    func getDatesFor24Hours() -> [Date] {
        var dates: [Date] = []
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: self)
        
        for hour in 0..<24 {
            let nextHour = calendar.date(byAdding: .hour, value: hour, to: startOfDay)!
            dates.append(nextHour)
        }
        return dates
    }

    func hoursOfDay(isYesterday: Bool = false) -> [Date] {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        let startOfDay = calendar.startOfDay(for: isYesterday ? Date.yesterday : self)
                var hour = calendar.date(bySetting: .hour, value: 0, of: startOfDay)!
                var array = [Date]()
                for _ in 0..<24 {
                    array.append(hour)
                    hour = calendar.date(byAdding: .hour, value: 1, to: hour)!
                }
                return array
      }
    
    func hoursOfCurrentDay(isYesterday: Bool = false) -> [Date] {
        var calendar = Calendar.current
        calendar.timeZone = .current
        let startOfDay = calendar.startOfDay(for: isYesterday ? Date.yesterday : self)
                var hour = calendar.date(bySetting: .hour, value: 0, of: startOfDay)!
                var array = [Date]()
                for _ in 0..<24 {
                    array.append(hour)
                    hour = calendar.date(byAdding: .hour, value: 1, to: hour)!
                }
                return array
      }

    static func yesterdayHours() -> [Date] {
        let calendar = Calendar(identifier: .gregorian)
        let startOfDay = calendar.startOfDay(for: Date.yesterday)
           var dates: [Date] = []
           for hour in 0..<24 {
               let hourDate = calendar.date(byAdding: .hour, value: hour, to: startOfDay)!
               let hourDateInTimeZone = hourDate.addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
               dates.append(hourDateInTimeZone)
           }
           return dates
    }

    func isSameWeek(as date: Date, in timeZone: TimeZone = .current) -> Bool {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(abbreviation: "UTC")!//timeZone
        calendar.firstWeekday = 2
        
        // Convert both dates to the same time zone
        let selfInTimeZone = calendar.dateComponents(in: timeZone, from: self)
        let dateInTimeZone = calendar.dateComponents(in: timeZone, from: date)
        
        // Get the start of the week for each date
        let selfStartOfWeek = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: selfInTimeZone.date!))!
        let dateStartOfWeek = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: dateInTimeZone.date!))!
        
        // Get the end of the week for each date
        let selfEndOfWeek = calendar.date(byAdding: .day, value: 6, to: selfStartOfWeek)!
        let dateEndOfWeek = calendar.date(byAdding: .day, value: 6, to: dateStartOfWeek)!
        
        // Check if the dates are in the same week
        if selfStartOfWeek <= dateEndOfWeek && selfEndOfWeek >= dateStartOfWeek {
            return true
        }
        
        // Adjust the end of the week if it falls on Sunday
        if calendar.component(.weekday, from: selfEndOfWeek) == 1 {
            let nextDay = calendar.date(byAdding: .day, value: 1, to: selfEndOfWeek)!
            if nextDay <= dateEndOfWeek && nextDay >= dateStartOfWeek {
                return true
            }
        }
        
        return false
    }

    func isInSameYear(as date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
    func isInSameMonth(as date: Date) -> Bool { isEqual(to: date, toGranularity: .month) }
    func isInSameWeek(as date: Date) -> Bool { isEqual(to: date, toGranularity: .weekOfYear) }
}

extension Date {
    func monthsFromNow() -> Int {
        let calendar = Calendar.current
        let currentDate = Date()
        
        let components = calendar.dateComponents([.month], from: currentDate, to: self)
        guard let months = components.month else {
            return 0
        }
        
        return months
    }
}

extension Date {
    func startOfMonth() -> Date! {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month], from: self)
        return calendar.date(from: components)
    }
}
