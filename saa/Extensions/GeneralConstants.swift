//
//  GeneralConstants.swift
//  saa
//
//

import Foundation

struct GeneralConstants {
    static var placeholderBarData: [ChartBarData] {
        var barData: [ChartBarData] = []
        
        Filters.allCases.forEach { filter in
            switch filter {
            case .live:
                barData.append(ChartBarData(type: .live(visitors: 0)))
            case .today:
                let todaySales = ChartBarData(type: .todaySales(total: 0, orders: 0), itemsCount: 24)
                let todaySessions = ChartBarData(type: .todaySessions(sessions: 0, visitors: 0), minValue: 2, itemsCount: 24)
                barData.append(contentsOf: [todaySales, todaySessions])
            case .yesterday:
                let todaySales = ChartBarData(type: .yesterdaySales(total: 0, orders: 0), itemsCount: 24)
                let todaySessions = ChartBarData(type: .yesterdaySessions(sessions: 0, visitors: 0), itemsCount: 24)
                barData.append(contentsOf: [todaySales, todaySessions])
            case .thisWeek:
                let todaySales = ChartBarData(type: .weekSales(total: 0, orders: 0), itemsCount: 24)
                let todaySessions = ChartBarData(type: .weekSessions(sessions: 0, visitors: 0), itemsCount: 24)
                barData.append(contentsOf: [todaySales, todaySessions])
            case .thisMonth:
                let todaySales = ChartBarData(type: .weekSales(total: 0, orders: 0), itemsCount: 30)
                let todaySessions = ChartBarData(type: .weekSessions(sessions: 0, visitors: 0), itemsCount: 30)
                barData.append(contentsOf: [todaySales, todaySessions])
            }
        }
        return barData
    }
}
