//
//  ColorExtensions.swift
//  saa
//
//

import Foundation
import SwiftUI

extension Color {
    static var searchBarColor = Color(#colorLiteral(red: 0.9647731185, green: 0.9645989537, blue: 0.9690193534, alpha: 1))
    static var placeholderText = Color(#colorLiteral(red: 0.4290205836, green: 0.4432878792, blue: 0.4562780261, alpha: 1))
    static var blurWhite = Color(#colorLiteral(red: 0.9685427547, green: 0.9686817527, blue: 0.9685124755, alpha: 1))
    static var appGreen = Color("appGreen")
    static var appPurple = Color("appPurple")
    static var lightAppGreen = Color("lightAppGreen")
    static var dividerColor = Color(#colorLiteral(red: 0.9333333373, green: 0.9333333373, blue: 0.9333333373, alpha: 1))
    static var appBlue = Color(#colorLiteral(red: 0.1727294624, green: 0.4316177368, blue: 0.797958374, alpha: 1))
    static var barColor = Color(#colorLiteral(red: 0, green: 0.6212709546, blue: 0.6460217834, alpha: 1))
    static var loaderColor = Color(#colorLiteral(red: 0.2519510984, green: 0.7712527514, blue: 0.8337435722, alpha: 1))
}
