//
//  DoubleExtensions.swift
//  saa
//
//

import Foundation

extension Double {
    func placeholderValue() -> Double {
        if self <= 1000 {
            return (self * 3) / 100
        }
        
        var valueToDivide = 1000
        
        if self >= 10000 {
            valueToDivide = 10000
        }

        var define = Double(self) / Double(valueToDivide)
        var new = define.roundToHalf()
        new *= 1000
        
        return (new * 3) / 100
    }

    func roundToHalf() -> Double {
        return (self * 2).rounded(.toNearestOrAwayFromZero) / 2
    }
    
    var stripeZeros: String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = ","
        formatter.decimalSeparator = "."
        formatter.minimumFractionDigits = 4
        formatter.maximumFractionDigits = 2
        return String(formatter.string(from: number) ?? "")
    }
    
    var stipeZerosWithKFormat: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.decimalSeparator = "."
        formatter.minimumFractionDigits = 4
        formatter.maximumFractionDigits = 2
        if self >= 1000 {
            let value = self / 1000.0
            return "\(formatter.string(from: NSNumber(value: value)) ?? "")K"
        } else {
            return "\(formatter.string(from: NSNumber(value: self)) ?? "")"
        }
        
    }
}

extension String {
    var isToday: Bool {
        // Get the current date and extract the weekday component
        let date = Date()
        let calendar = Calendar.current
        let currentWeekday = calendar.component(.weekday, from: date)
        
        // Define a mapping of weekday abbreviations to their corresponding numeric values
        let weekdayMap: [String: Int] = ["Su": 1, "Mo": 2, "Tu": 3, "We": 4, "Th": 5, "Fr": 6, "Sa": 7]
        
        // Check if the current weekday matches the weekday abbreviation
        if let targetWeekday = weekdayMap[self] {
            return currentWeekday == targetWeekday
        } else {
            return false // Invalid weekday abbreviation
        }
    }
}
