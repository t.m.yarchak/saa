//
//  ViewModifiers.swift
//  saa
//
//

import SwiftUI

struct RoundedCorner: Shape {
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

struct PlaceholderRow: View {
    var body: some View {
        Capsule()
            .fill(Color.gray.opacity(0.3))
            .frame(height: 10)
         
    }
}

struct PlainProgressView: View {
    var body: some View {
        ZStack {
            Color.dividerColor.ignoresSafeArea(.all, edges: .bottom)
               
            ProgressView()
                .scaleEffect(1.4)
        }
        .offset(y: 40)
    }
}

struct BorderedButton<Label: View>: View {
    let action: () -> Void
    @ViewBuilder var label: Label
    
    var body: some View {
        Button(action: action) {
            label
            .buttonStyle(.plain)
            .padding(.horizontal, 12)
            .padding(.vertical, 7)
            .background(Color.white)
            .overlay(
                RoundedRectangle(cornerRadius: 5)
                    .stroke()
                    .foregroundColor(.gray)
            )
        }
    }
}

struct DashboardButton: View {
    let selection: Filters

    let action: () -> Void
    var body: some View {
        Button(action: action) {
            ZStack {
                Color.lightAppGreen
                Text(selection.buttonTitle)
                    .font(.system(size: 16, weight: .semibold))
                    .foregroundColor(.appGreen)
                    .padding(.vertical, 13)
            }
        }
        .cornerRadius(8)
    }
}
