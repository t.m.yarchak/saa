//
//  IntExtensions.swift
//  saa
//
//

import Foundation

extension Int {
    func splitInteger(_ constant: Int) -> [Int] {
        guard self >= constant * 4 else {
            return [0, 0, constant, 0] // The number is too small to be split into four parts
        }
        
        var remaining = self
        var parts: [Int] = []
        
        for _ in 0..<3 {
            let maxPart = remaining - (12 * (3 - parts.count))
            let part = Int.random(in: 12...maxPart)
            parts.append(part)
            remaining -= part
        }
        
        parts.append(remaining)
        return parts
    }

    func placeholderValue() -> Int {
        if self <= 1000 {
            return Int((self * 3) / 100)
        }
        
        var valueToDivide = 1000
        
        if self >= 10000 {
            valueToDivide = 10000
        }
        
        var define = Double(self) / Double(valueToDivide)
        var new = define.roundToHalf()
        new *= 1000
        
        return (Int(new) * 3) / 100
    }
    
    func generateRandomSliceArray() -> [Int] {
        var amount = self
        let constant = 1
        var amountArray = [Int]()

        while amount != 0 {
            var randomPiece = Int.random(in: 1...amount)
            
            if randomPiece == amount && amountArray.isEmpty {
                randomPiece /= 3
            }

            amountArray.append(randomPiece * constant)
            amount -= randomPiece
        }
       return amountArray
    }
    
    var comaK: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = ","
        return numberFormatter.string(from: NSNumber(value: self))!
    }

    var isPossitive: Bool {
        return self > 0
    }

    var KFormatted: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 1
        formatter.decimalSeparator = "."
        if self >= 1000 {
            let value = Double(self) / 1000.0
            return "\(formatter.string(from: NSNumber(value: value)) ?? "")K"
        } else {
            return "\(formatter.string(from: NSNumber(value: self)) ?? "")"
        }
    }
}
