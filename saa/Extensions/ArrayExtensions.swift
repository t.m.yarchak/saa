//
//  ArrayExtensions.swift
//  saa
//
//

import Foundation

extension Array where Element == Int {
    func pad(to length: Int, with element: Element) -> [Element] {
        guard self.count <= length else { return self }
        let paddedArray = self + Array(repeating: element, count: length - count)
        return Array(paddedArray.prefix(length))
    }
}

extension Array where Element == Double {
    func pad(to length: Double, with element: Element) -> [Element] {
        let paddedArray = self + Array(repeating: element, count: Int(length) - count)
        return Array(paddedArray.prefix(Int(length)))
    }
}
