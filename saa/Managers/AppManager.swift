//
//  AppManager.swift
//  saa
//
//

import Combine
import Foundation

class AppManager: ObservableObject {
    private var cancellables = Set<AnyCancellable>()
    
    let infoDAO: InfoDAO
    
    @Published var datesPreset: [SaleDay] = []
    @Published var user: CurrentUser?
    @Published var chartBarData: [ChartBarData] = []
    
    init(infoDAO: InfoDAO) {
        self.infoDAO = infoDAO
        self.addSubscribers()
    }
    
    func addSubscribers() {
        infoDAO.$datesPreset
            .assign(to: \.datesPreset, on: self)
            .store(in: &cancellables)

        infoDAO.$currentUser
            .assign(to: \.user, on: self)
            .store(in: &cancellables)
        
        infoDAO.$chartBarData
            .assign(to: \.chartBarData, on: self)
            .store(in: &cancellables)
    }
    
    func updateDatesPreset(dates: [SaleDay]) {
        self.infoDAO.updateDatesPreset(dates: dates)
    }
    
    func generateDay(with day: Date) {
        self.infoDAO.generateDay(with: day)
    }
}

import Combine

// Prevents memory leaks on sink
extension Publisher where Failure == Never {
    func assign<Root: AnyObject>(to keyPath: ReferenceWritableKeyPath<Root, Output>, on root: Root) -> AnyCancellable {
        sink { [weak root] in
            root?[keyPath: keyPath] = $0
        }
    }
}
