//
//  PreviewManager.swift
//  saa
//
//

import SwiftUI

extension PreviewProvider {
    static var dev: PreviewManager {
        return PreviewManager.shared
    }
}

class PreviewManager {
    static let shared = PreviewManager()

    private init() { }

    lazy var managersProvider = ManagersProvider()
    lazy var appCoordinator = AppCoordinator(managersProvider: managersProvider)
}
