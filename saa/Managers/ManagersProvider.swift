//
//  ManagersProvider.swift
//  saa
//
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

final class ManagersProvider {
    let appManager: AppManager
    
    init() {
        let store = Firestore.firestore()
        let infoDAO = InfoDAO(store: store)

        self.appManager = AppManager(infoDAO: infoDAO)
        self.addSubscribers()
    }

    func addSubscribers() { }
}

