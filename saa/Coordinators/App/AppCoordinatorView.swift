//
//  AppCoordinatorView.swift
//  saa
//
//

import SwiftUI

enum TabSelection: String {
    case home = "Home"
    case orders = "Orders"
    case products = "Prooducts"
    case store = "Store"
}

struct AppCoordinatorView: View {
    @ObservedObject var coordinator: AppCoordinator
    @State var tabSelected: TabSelection = .home
    @State private var isShowingDatePicker = false
    @State private var selectedDate = Date()

    init(coordinator: AppCoordinator) {
        self.coordinator = coordinator
        self.setupTabBarAppearance()
    }

    var body: some View {
        TabView(selection: $tabSelected) {
            NavigationView {
                HomeView(viewModel: coordinator.homeViewModel)
                    .fullScreenCover(item: $coordinator.analyticsViewModel, content: analyticsView)
            }.clipped()
            
            .tag(TabSelection.home)
            .tabItem({
                Image("Home")
                    .renderingMode(.template)
            })
            
            Color.white
                .tag(TabSelection.orders)
                .tabItem({
                    Image("Orders")
                        .renderingMode(.template)
                })
            
            Color.white
                .tag(TabSelection.products)
                .tabItem({
                    Image("Products")
                        .renderingMode(.template)
                })

            Color.white
                .tag(TabSelection.store)
                .tabItem({
                    Image("Store")
                        .renderingMode(.template)
                })
        }
        .dateAlert(isPresented: $isShowingDatePicker, title: "GENERATE DAY", date: $selectedDate, action: onDateAlert)
        .accentColor(Color.appGreen)
        .onChange(of: tabSelected, perform: onTabChange)
    }
    
    func onDateAlert(_ date: Date) {
        self.coordinator.generateDay(with: date)
        self.selectedDate = Date()
    }
    
    func onTabChange(_ tab: TabSelection) {
        if tab == .store {
            withAnimation(.easeIn(duration: 0.1)) {
                self.isShowingDatePicker.toggle()
            }
        }
        tabSelected = .home
    }

    @ViewBuilder
    private func analyticsView(_ viewModel: AnalyticsViewModel) -> some View {
        AnalyticsView(viewModel: viewModel)
    }
}

struct AppCoordinatorView_Previews: PreviewProvider {
    static var previews: some View {
        AppCoordinatorView(coordinator: dev.appCoordinator)
    }
}
