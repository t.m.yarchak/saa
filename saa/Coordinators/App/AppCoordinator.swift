//
//  AppCoordinator.swift
//  saa
//
//

import Combine
import Foundation

final class AppCoordinator: BaseCoordinator {
    @Published var homeViewModel: HomeViewModel!
    @Published var analyticsViewModel: AnalyticsViewModel?

    override init(managersProvider: ManagersProvider) {
        super.init(managersProvider: managersProvider)

        self.homeViewModel = .init(coordinator: self)
    }

    func openAnalytics(selection: Filters) {
        self.analyticsViewModel = .init(coordinator: self, selection: selection)
    }

    func closeAnalytics() {
        self.analyticsViewModel = nil
    }
    
    func generateDay(with day: Date) {
        self.managersProvider.appManager.generateDay(with: day)
    }
}
