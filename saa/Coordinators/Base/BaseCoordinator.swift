//
//  BaseCoordinator.swift
//  saa
//

import Combine
import SwiftUI

enum CoordinatorStartType {
}

enum CoordinatorStopType {
}

class BaseCoordinator: ObservableObject, Identifiable {
    var cancellables = Set<AnyCancellable>()

    let managersProvider: ManagersProvider

    init(managersProvider: ManagersProvider) {
        self.managersProvider = managersProvider
        self.addSubscribers()
    }

    func addSubscribers() {}

    func start(_ type: CoordinatorStartType) {}
    
    func stop(_ type: CoordinatorStopType) {}
}
