//
//  SearchBarView.swift
//  saa
//
//

import SwiftUI

struct SearchBarView: View {
    @Binding var searchText: String

    var body: some View {
        HStack(spacing: 12) {
            Image("searchIcon")

            TextField("Go to...", text: $searchText)
                .foregroundColor(Color.black)
                .font(.system(size: 16, weight: .regular))
                .accentColor(Color.placeholderText)
                .disableAutocorrection(true)
                .overlay(searchIconView, alignment: .trailing)
        }
        .font(.system(size: 16))
        .padding(.horizontal, 16)
        .padding(.vertical, 13)
        .background(RoundedRectangle(cornerRadius: 6).fill(Color.searchBarColor))
    }

    var searchIconView: some View {
        Image(systemName: "xmark.circle.fill")
            .padding(10)
            .offset(x: 10)
            .opacity(searchText.isEmpty ? 0 : 1)
            .onTapGesture {
                searchText.removeAll()
            }
    }
}
