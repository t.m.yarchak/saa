//
//  LoadingWheelView.swift
//  saa
//
//

import SwiftUI

struct LoadingWheelView: View {
    @State private var isLoading = false
    
       var body: some View {
           ZStack {
               Color.clear
               Circle()
                   .trim(from: 0, to: 0.7)
                   .stroke(Color.loaderColor, lineWidth: 3)
                   .frame(width: 40, height: 40)
                   .rotationEffect(Angle(degrees: isLoading ? 360 : 0))
                   .animation(Animation.linear(duration: 0.5).repeatForever(autoreverses: false))
                   .onAppear() {
                       self.isLoading = true
                   }
           }
       }
}

struct LoadingWheelView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingWheelView()
    }
}
