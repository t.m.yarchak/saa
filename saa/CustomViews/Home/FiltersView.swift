//
//  FiltersView.swift
//  saa
//
//

import SwiftUI

struct FiltersView: View {
    @Binding var text: String
    let selectedFilter: Filters
    let onSelect: (Filters) -> Void
    
    @State var animate = false

    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            ScrollViewReader { scrollViewProxy in
                HStack(spacing: 8) {
                    ForEach(Filters.allCases, id: \.self) { filter in
                        HStack {
                            if case .live = filter {
                                Color.red
                                    .clipShape(Circle())
                                    .frame(width: 6, height: 6)
                                    .opacity(self.animate ? 0.3 : 1)
                                    .animation(Animation.linear(duration: 0.7).repeatForever())
                                    .onAppear {
                                        self.animate = true
                                    }
                            }
                            
                            Text("\(filter.title)")
                                .font(.system(size: 14, weight: .regular))
                                .foregroundColor(selectedFilter == filter ? .appGreen : .black)
                        }
                        .padding(.vertical, 8)
                        .padding(.horizontal, 10)
                        .background(selectedFilter == filter ? Color.lightAppGreen : Color.searchBarColor)
                        .cornerRadius(6)
                        .onTapGesture {
                            self.onSelect(filter)

                            withAnimation {
                                scrollViewProxy.scrollTo(filter, anchor: .center)
                                
                            }
                        }
                        .onChange(of: selectedFilter) { newValue in
                            withAnimation {
                                scrollViewProxy.scrollTo(newValue, anchor: .center)
                            }
                        }
                    }
                }
                .padding(.horizontal, 15)
            }
        }
    }
}
