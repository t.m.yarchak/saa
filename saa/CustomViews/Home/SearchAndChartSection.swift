//
//  SearchAndChartSection.swift
//  saa
//
//

import SwiftUI

struct SearchAndChartSection: View {
    @State var animation: Animation?
    @Binding var activeFilter: Filters
    @Binding var searchText: String
    @Binding var chartImage: ChartImageEnum
    let placeholderLoad: PlaceholderLoad
    let saleDay: SaleDay?
    let chartBarData: [ChartBarData]
    let load: PlaceholderLoad
    let onSelect: (Filters) -> Void
    

    let action: () -> Void
    var body: some View {
        VStack(spacing: 0) {
            SearchBarView(searchText: $searchText)
                .padding(.horizontal, 15)
            
            FiltersView(text: $searchText, selectedFilter: activeFilter, onSelect: onSelect)
                .padding(.vertical, 16)
            
            CarouselView(selection: activeFilter, chartBarData: chartBarData, spacing: 5, widthOfHiddenCards: -5, buttonAction: action)
                .transition(.slide)
                .animation(animation)
                .padding(.vertical, 15)
                .onAppear {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        self.animation = .spring()
                    })
                }
            
            Color.dividerColor
                .frame(height: 8)
        }
    }
}
