//
//  AbandonedCheckouts.swift
//  saa
//
//

import SwiftUI

struct AbandonedCheckouts: View {
    let leftBehind: String
    var body: some View {
        VStack(spacing: 30) {
            HStack(spacing: 20) {
                Text("GOAL")
                    .font(.system(size: 14, weight: .bold))
                    .foregroundColor(.white)
                    .padding(5)
                    .background(Color.appGreen)
                    .cornerRadius(2)
                
                Text("Reduce abandoned checkouts")
                    .font(.system(size: 16, weight: .semibold))
                
                Spacer()
            }
            .padding(.horizontal, 20)
            
            VStack(spacing: 4) {
                Image("shoppingCart")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 22, height: 22)
                
                Text("LAST 30 DAYS")
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(.secondary)
                    .padding(.top, 14)
                
                Text("Customers left behind $\(leftBehind) during checkout")
                    .font(.system(size: 22, weight: .semibold))
                    .multilineTextAlignment(.center)
            }
            .padding(.top, 25)
            .padding(.horizontal, 15)
            
            Text("When a customers exit your checkout before make an order")
                .font(.system(size: 14, weight: .regular))
                .foregroundColor(.secondary)
                .multilineTextAlignment(.center)
                .padding(.horizontal, 15)
        }
        .padding(.vertical, 15)
        .overlay(
            VStack {
                Spacer()
                Color.dividerColor
                    .frame(height: 8)
            }
        )
    }
}
