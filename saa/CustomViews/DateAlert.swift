//
//  DateAlert.swift
//  saa
//
//

import SwiftUI

struct DateAlert: ViewModifier {
    @Binding var isPresented: Bool
    let title: String
    @Binding var date: Date
    let action: (Date) -> Void
    func body(content: Content) -> some View {
        ZStack(alignment: .center) {
            content
                .disabled(isPresented)
            if isPresented {
                BlurView()
                    .ignoresSafeArea()
                    .opacity(0.8)
                VStack() {
                    Text(title).font(.headline).padding()
                    DatePicker("SELECTION", selection: $date, displayedComponents: [.date])
                        .padding()

                    Divider()
                    HStack{
                        Spacer()
                        Button {
                            withAnimation {
                                isPresented.toggle()
                            }
                        } label: {
                            Text("Cancel")
                        }
                        Spacer()
                        Divider()
                        Spacer()
                        Button() {
                            action(date)
                            withAnimation {
                                isPresented.toggle()
                            }
                        } label: {
                            Text("Done")
                        }
                        Spacer()
                    }
                }
                .background(Color.white)
                .frame(width: 300, height: 190)
                .cornerRadius(20)
                .shadow(radius: 3)
            }
        }
    }
}

extension View {
    public func dateAlert(
        isPresented: Binding<Bool>,
        title: String,
        date: Binding<Date>,
        action: @escaping (Date) -> Void
    ) -> some View {
        self.modifier(DateAlert(isPresented: isPresented, title: title, date: date, action: action))
    }
}
