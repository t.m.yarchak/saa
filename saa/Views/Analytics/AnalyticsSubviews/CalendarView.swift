//
//  CalendarView.swift
//  saa
//
//

import SwiftUI

struct DateValue: Identifiable{
    var id = UUID().uuidString
    var day: Int
    var date: Date
}

struct DatePView: View {
    @State var currentDate = Date()
    @Binding var firstSelection: Date?
    @Binding var secondSelection: Date?
    
    let calendarType: CalendarSelection
    @Binding var selectionForMain: CalendarOptions
    @Binding var selectionForCompare: CompareOptions
    
    @State var currentMonth: Int = 0

    var topStack: some View {
        HStack {
            Button {
                currentMonth -= 1
            } label: {
                Image(systemName: "arrow.left")
                    .foregroundColor(.gray)
            }
            Spacer()

            HStack(spacing: 3) {
                Text(extraDate()[1])
                Text(extraDate()[0])
            }
            .font(.system(size: 15, weight: currentDate.isSameMonth(as: Date()) ? .semibold : .regular))
            
            Spacer()

            Button {
                currentMonth += 1
            } label: {
                Image(systemName: "arrow.right")
                    .foregroundColor(.gray)
            }
        }
    }
    
    var body: some View {
        
        VStack(spacing: 10) {
            let days: [String] = ["Mo","Tu","We","Th","Fr","Sa", "Su"]
            topStack

            HStack(spacing: 0) {
                ForEach(days, id: \.self) { day in
                    Text(day)
                        .font(.system(size: 12, weight: day.isToday && currentMonth == 0 ? .bold : .regular))
                        .frame(maxWidth: 40)
                }
            }

            let columns = Array(repeating: GridItem(.fixed(40), spacing: 0), count: 7)

            LazyVGrid(columns: columns, spacing: 0) {
                ForEach(extractDate()) { value in
                    CardView(value: value)
                        .cornerRadius(secondSelection == nil ? 3 : 14, corners: cornersFoRadius(date: value.date))
                        .onTapGesture {
                            guard !value.date.isInFuture else { return }                      

                            if secondSelection != nil {
                                firstSelection = value.date
                                secondSelection = nil
                            }

                            if isSameDay(date1: value.date, date2: firstSelection) {
                                firstSelection = value.date
                                secondSelection = nil
                                return
                            }

                            if let second = secondSelection, isSameDay(date1: value.date, date2: second) {
                                secondSelection = nil
                                firstSelection = value.date
                            }

                            if value.date.stripTime < (firstSelection ?? Date()).stripTime {
                                firstSelection = value.date
                                secondSelection = nil
                            } else if value.date.stripTime > (firstSelection ?? Date()).stripTime && secondSelection == nil {
                                secondSelection = value.date
                            }
                            
                            switch calendarType {
                            case .main:
                                self.selectionForMain = (firstSelection ?? Date()).isInToday && secondSelection == nil ? .today : .custom
                            case .comparison:
                                self.selectionForCompare = .custom
                            }
                        }
                    
                }
            }
        }
        .onChange(of: firstSelection, perform: { newValue in
            self.currentMonth = self.months(from: newValue ?? Date())
        })
        .onChange(of: currentMonth) { newValue in
            currentDate = getCurrentMonth()
        }
    }
    
    func cornersFoRadius(date: Date) -> UIRectCorner {
        guard let secondSelection = secondSelection else { return .allCorners }

        if isSameDay(date1: date, date2: firstSelection) {
            return [.bottomLeft, .topLeft]
        }
        if isSameDay(date1: date, date2: secondSelection) {
            return [.bottomRight, .topRight]
        }
        return []
    }

    @ViewBuilder
    func CardView(value: DateValue)->some View{
        ZStack {
            Color.appBlue
                .opacity(opacityForItem(date: value.date))
            Text("\(value.day)")
                .lineLimit(1)
                .font(.system(size: 12, weight: styleForItem(date: value.date)))
                .foregroundColor(colorForText(date: value.date))
        }
        .frame(width: 40, height: 28)
        .opacity(value.day != -1 ? 1 : 0)
    }

    func colorForText(date: Date) -> Color {
        if isSameDay(date1: date, date2: firstSelection) || isSameDay(date1: date, date2: secondSelection) {
            return .white
        }
        return date.isInFuture ? .gray : .black
  
    }
    
    func styleForItem(date: Date) -> Font.Weight {
        if isSameDay(date1: date, date2: firstSelection) || date.isInToday {
            return .bold
        }
        
        if let second = secondSelection, isSameDay(date1: date, date2: second) {
            return .bold
        }

        return .regular
    }
    
    func opacityForItem(date: Date) -> Double {
        if isSameDay(date1: date, date2: firstSelection) {
            return 1
        }
        
        if let second = secondSelection, isSameDay(date1: date, date2: second) {
            return 1
        }
        
        if let second = secondSelection, date.stripTime > (firstSelection ?? Date()) && date.stripTime < second {
            return 0.2
        }
        
        return 0
    }

    func isInRange(dateValue: Date) -> Bool {
        guard let secondDate = secondSelection?.stripTime else {
            return false
        }

        return dateValue.stripTime < secondDate && dateValue.stripTime > secondDate.stripTime
    }

    func isSameDay(date1: Date, date2: Date?)->Bool {
        guard let date2 = date2 else { return false }
        let calendar = Calendar.current
        
        
        return calendar.isDate(date1, inSameDayAs: date2)
    }
    
    // extrating Year And Month for display...
    func extraDate() -> [String] {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: currentDate) - 1
        let year = calendar.component(.year, from: currentDate)
        
        return ["\(year)", calendar.monthSymbols[month]]
    }
    
    func getCurrentMonth() -> Date {
        let calendar = Calendar.current

        guard let currentMonth = calendar.date(byAdding: .month, value: self.currentMonth, to: Date()) else {
            return Date()
        }
        return currentMonth
    }
    
    func getCurrentMonth(toDate: Date) -> Date {
        let calendar = Calendar.current

        guard let currentMonth = calendar.date(byAdding: .month, value: self.currentMonth, to: toDate) else {
            return Date()
        }
        return currentMonth
    }
    
    func months(from date: Date) -> Int {
        let calendar = Calendar.current
        let currentDate = Date()

        let components = calendar.dateComponents([.month], from: date.startOfMonth(), to: currentDate.startOfMonth())
        guard let months = components.month else {
            return 0
        }

        let isFutureDate = date > currentDate
        return isFutureDate ? months : -months
    }
    
    func extractDate() -> [DateValue] {
        let calendar = Calendar.current
        let currentMonth = getCurrentMonth()

        var days = currentMonth.getAllDates().compactMap { date -> DateValue in
            let day = calendar.component(.day, from: date)
            return DateValue(day: day, date: date)
        }

        let firstWeekday = calendar.component(.weekday, from: days.first!.date)
        
        guard firstWeekday > 1 else { return days }
        
        (1..<firstWeekday - 1).forEach { int in
            days.insert(DateValue(day: -1, date: Date()), at: 0)
        }

        return days
    }
}
