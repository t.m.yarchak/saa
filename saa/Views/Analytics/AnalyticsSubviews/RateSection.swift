//
//  RateSection.swift
//  saa
//
//

import SwiftUI

struct RateSection: View {
    @Binding var placeholderLoad: PlaceholderLoad
    let saleDay: SaleDay?
    let toCompare: SaleDay?
    
    var ratePersents: Int {
        guard let saleDay = saleDay, let toCompare = toCompare else { return 0 }
        guard saleDay.conversionRate != 0 && toCompare.conversionRate != 0 else { return 0 }
        return Int(((saleDay.conversionRate - toCompare.conversionRate) / abs(toCompare.conversionRate)) * 100)
    }

    var placeholder: some View {
        HStack(alignment: .bottom) {
            VStack(alignment: .leading, spacing: 16) {
                PlaceholderRow().frame(width: UIScreen.screenWidth / 3)
                PlaceholderRow()
                PlaceholderRow()
            }
            .frame(width: UIScreen.screenWidth / 2)

            Spacer()

            VStack(alignment: .leading, spacing: 16) {
                PlaceholderRow()
                PlaceholderRow()
            }
            .frame(width: UIScreen.screenWidth / 5)
        }
        .padding(.vertical, 18)
        .padding(.horizontal, 15)
        .background(Color.white)
    }
    
    var body: some View {
        if placeholderLoad == .firstPlaceholder {
            placeholder
            placeholder
            placeholder
        } else if placeholderLoad == .secondPlaceholder {
            placeholder
            placeholder
        } else {
            section
        }
    }

    var section: some View {
        VStack(alignment: .leading, spacing: 21) {
            VStack(alignment: .leading, spacing: 9) {
                Text("Conversion rate")
                    .foregroundColor(.blue)
                    .font(.system(size: 15, weight: .semibold))
                    .padding(.bottom, 3)
                    .overlay(
                        VStack {
                            Spacer()
                            Line()
                                .stroke(style: StrokeStyle(lineWidth: 2, dash: [2]))
                                .foregroundStyle(Color.gray.opacity(0.5))
                                .frame(height: 1)
                        }
                    )

                HStack {
                    Text("\((saleDay?.conversionRate ?? 0).stripeZeros)" + "%")
                        .font(.system(size: 16, weight: .medium))
                    Spacer()
                    
                    if ratePersents != 0 {
                        HStack(spacing: 2) {
                            Image(ratePersents.isPossitive ? "arrowUp" : "arrowDown")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 8, height: 8)
                            
                            Text("\(ratePersents)")
                                .font(.system(size: 14, weight: .semibold))
                                .foregroundColor(ratePersents.isPossitive ? .appGreen : .red)
                        }
                    }
                }
            }

            Text("Conversion funnel")
                .font(.system(size: 12, weight: .regular))

            VStack(alignment: .leading, spacing: 16) {
                ForEach(RateItemSectionType.allCases, id: \.self) { type in
                    RateItemSection(type: type, saleDay: saleDay, toCompare: toCompare)
                }
            }

        }
        .padding(.vertical, 18)
        .padding(.horizontal, 15)
        .background(Color.white)
    }
}

struct RateItemSection: View {
    let type: RateItemSectionType
    let saleDay: SaleDay?
    let toCompare: SaleDay?
    
    var values: SectionValues? {
        saleDay?.valuesForSection(type: type)
    }
    
    var percents: Int {
        guard let saleDay = saleDay, let toCompare = toCompare else { return 0 }
        
        let first: Double
        let second: Double
        
        first = Double(saleDay.valuesForSection(type: type).main)
        second = Double(toCompare.valuesForSection(type: type).main)
        
        if first == 0 || second == 0 {
            return 100
        }
        
        return Int(((first - second) / abs(second)) * 100)
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            VStack(alignment: .leading, spacing: 3) {
                Text(type.title)
                    .font(.system(size: 14, weight: .regular))
                Text("\((values?.additional ?? 0).comaK) sessions")
                    .font(.system(size: 13, weight: .regular))
                    .foregroundColor(.placeholderText)
            }
            HStack(spacing: 8) {
                Text("\((values?.main ?? 0).stripeZeros)%")
                    .font(.system(size: 14, weight: .regular))
                    
                Spacer()
                if percents != 0 {
                    HStack(spacing: 2) {
                        Image(percents.isPossitive ? "arrowUp" : "arrowDown")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 8, height: 8)
                        
                        Text("\(abs(percents))%")
                            .font(.system(size: 12, weight: .semibold))
                            .foregroundColor(percents.isPossitive ? .appGreen : .red)
                    }
                }
            }
        }
    }
}
