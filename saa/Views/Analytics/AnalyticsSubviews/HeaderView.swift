//
//  HeaderView.swift
//  saa
//
//

import SwiftUI

struct HeaderView: View {
    let action: () -> Void
    var body: some View {
        VStack {
            HStack {
                Button(action: action) {
                    Text("Done")
                        .font(.system(size: 16, weight: .medium))
                        .foregroundColor(.appGreen)
                }
                .padding(.horizontal, 15)
                .padding(.bottom, 11)
                
                Spacer()
            }
            Color.gray.opacity(0.5)
                .frame(height: 1)
        }
        .padding(.top, 10)
        .background(
             Color.white
                .ignoresSafeArea(.all, edges: .top)
            )
    }
}
