//
//  ChartSection.swift
//  saa
//
//

import SwiftUI

struct ChartSection: View {
    @Binding var placeholderLoad: PlaceholderLoad
    let type: ChartSectionType
    let saleDay: SaleDay?
    let saleDayToCompare: SaleDay?
    let selection: CalendarOptions
    let minItemCost: Double
    let mainSelectedDates: [Date]
    
    var persentegeDifference: Int {
        guard let saleDay = saleDay, let toCompare = saleDayToCompare else { return 0 }

        let first: Double
        let second: Double
        
        switch type {
        case .sales:
            first = saleDay.totalSales(minItemCost)
            second = toCompare.totalSales(minItemCost)
        case .orders:
            first = Double(saleDay.totalOrders)
            second = Double(toCompare.totalOrders)
        }

        guard first != 0 && second != 0 else { return 0 }
    
        return Int(((first - second) / abs(second)) * 100)
    }

    var valueString: String {
        switch type {
        case .sales:
            return "$" + "\((saleDay?.totalSales(minItemCost) ?? 0).stripeZeros)"
        case .orders:
            return "\((saleDay?.totalOrders ?? 0).comaK)"
        }
    }
    
    var value: Double? {
        switch type {
        case .sales:
            return saleDay?.totalSales(minItemCost)
        case .orders:
            guard let orders = saleDay?.totalOrders else {
                return nil
            }
            return Double(orders)
        }
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 40) {
            VStack(alignment: .leading, spacing: 9) {
                
                if placeholderLoad == .firstPlaceholder {
                    PlaceholderRow()
                        .frame(width: UIScreen.screenWidth / 2)
                } else {
                    VStack(spacing: 10) {
                        Text(type.title)
                            .foregroundColor(.blue)
                            .font(.system(size: 16, weight: .semibold))
                    }
                    .padding(.bottom, 3)
                    .overlay(
                        VStack {
                            Spacer()
                            Line()
                                .stroke(style: StrokeStyle(lineWidth: 2, dash: [2]))
                                .foregroundStyle(Color.gray.opacity(0.5))
                                .frame(height: 1)
                        }
                    )
                }

                if placeholderLoad == .loaded {
                    VStack(alignment: .leading, spacing: 22) {
                        HStack {
                            Text(valueString)
                                .font(.system(size: 16, weight: .medium))
                            
                            
                            if persentegeDifference != 0 {
                                HStack(spacing: 1) {
                                    Image(persentegeDifference.isPossitive ? "arrowUp" : "arrowDown")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 8, height: 8)
                                    
                                    Text("\(abs(persentegeDifference))%")
                                        .font(.system(size: 12, weight: .semibold))
                                        .foregroundColor(persentegeDifference.isPossitive ? .appGreen : .red)
                                }
                            }
                        }
                        
                        if type == .sales {
                            VStack(alignment: .leading, spacing: 12) {
                                Text("Online Store")
                                    .font(.system(size: 14, weight: .regular))
                                Text(valueString)
                                    .font(.system(size: 14, weight: .regular))
                            }
                        }
                    }
                }
            }
            .padding(.horizontal, 15)
            
            if placeholderLoad == .firstPlaceholder {
                LoadingWheelView()
                    .padding(.bottom, 25)
            } else if placeholderLoad == .secondPlaceholder {
                LoadingWheelView()
                    .padding(.bottom, 25)
            } else {
                VStack(alignment: .leading) {
                    Text(type.subtitle)
                        .foregroundColor(.blue)
                        .font(.system(size: 12, weight: .semibold))
                        .padding(.bottom, 2)
                        .overlay(
                            VStack {
                                Spacer()
                                Line()
                                    .stroke(style: StrokeStyle(lineWidth: 1, dash: [2]))
                                    .foregroundStyle(Color.gray.opacity(0.5))
                                    .frame(height: 1)
                            }
                        )
                        .padding(.horizontal, 15)
                        
                    
                    if let value = value, let saleDay = saleDay, value > 0 {
                        LineChartView(chartType: type, saleDay: saleDay, selection: selection, minItemCost: minItemCost, chartData: saleDay.chartData)
                            .frame(height: 120)
                            .padding(.vertical)
                    } else {
                        EmptyChartView(dates: mainSelectedDates)
                            .frame(height: 120)
                            .padding(.vertical)
                    }
                    
                    HStack {
                        Spacer()
                        
                        HStack {
                            
                            Capsule()
                                .foregroundColor(.appPurple)
                                .frame(width: 10, height: 1.5)
                            
                            Text(extractTitle())
                                .font(.system(size: 12, weight: .regular))
                                .foregroundColor(.gray)
                        }
                        .padding(.vertical, 4)
                        .padding(.trailing, 14)
                        .padding(.leading, 8)
                        .background(Color.dividerColor)
                        .cornerRadius(2)
                    }
                    .padding(.horizontal, 30)
                    
                }
            }
        }
        .padding(.vertical, 18)
        .background(Color.white)
    }
    
    func extractTitle() -> String {
        guard !mainSelectedDates.isEmpty else { return "" }

        if let first = mainSelectedDates.first, mainSelectedDates.count == 1 {
            return first.asTitleWithMonthString
        } else {
            guard let first = mainSelectedDates.first, let last = mainSelectedDates.last else {
                return ""
            }
            return first.asTitle(plusDate: last)
        }
    }
}
