//
//  CustomCalendarView.swift
//  saa
//
//

import SwiftUI

enum CompareOptions: String, CaseIterable {
    case noComparison = "No comparison"
    case previousPeriod = "Previous period"
    case firstQ = "1st Quarter"
    case secondQ = "2nd Quarter"
    case thirdQ = "3d Quarter"
    case fourthQ = "4th Quarter"
    case BFCM19 = "BFCM (2019)"
    case BFCM20 = "BFCM (2020)"
    case BFCM21 = "BFCM (2021)"
    case BFCM22 = "BFCM (2022)"
    case custom = "Custom"
    
    static var dataSource: [CompareOptions] {
        return CompareOptions.allCases.filter({ $0 != .custom })
    }
}

enum CalendarOptions: String, CaseIterable {
    case today = "Today"
    case yesterday = "Yesterday"
    case last7days = "Last 7 days"
    case last30days = "Last 30 days"
    case last90days = "Last 90 days"
    case lastMonth = "Last month"
    case lastYear = "Last year"
    case weekToDate = "Week to date"
    case monthToDate = "Month to date"
    case quarterToDate = "Quarter to date"
    case yearToDate = "Year to date"
    case firstQ = "1st Quarter"
    case secondQ = "2nd Quarter"
    case thirdQ = "3d Quarter"
    case fourthQ = "4th Quarter"
    case BFCM19 = "BFCM (2019)"
    case BFCM20 = "BFCM (2020)"
    case BFCM21 = "BFCM (2021)"
    case BFCM22 = "BFCM (2022)"
    case custom = "Custom"
    
    static var dataSource: [CalendarOptions] {
        return CalendarOptions.allCases.filter({ $0 != .custom })
    }
}

extension String {
    func toDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: self)
        
        if let date = date {
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            return year >= 2000 && year < 2100 ? date : nil
        }
        return nil
    }
}

struct CustomTextFieldStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
           .padding(1)
            .background(Color.gray)
            .cornerRadius(8)
    }
}

extension TextField {
    func customStyle() -> some View {
        self.modifier(CustomTextFieldStyle())
    }
}

struct CustomTextField: View {
    @Binding var text: String
    @Binding var date: Date?
    @State var active = false
    @Binding var selectionForMain: CalendarOptions
    @Binding var selectionForCompare: CompareOptions
    let calendarType: CalendarSelection

    var body: some View {
        TextField("", text: $text, onEditingChanged: { active = $0 }, onCommit: {
            if let date = text.toDate() {
               self.date = date
                switch calendarType {
                case .main:
                    self.selectionForMain = .custom
                case .comparison:
                    self.selectionForCompare = .custom
                }
            } else {
                DispatchQueue.main.async {
                    self.text = self.date?.asString ?? "YYYY-MM-DD"
                }
            }
            self.active = false
        })
        .font(.system(size: 15, weight: .regular))
        .borderStyle()
        .overlay(
            ZStack(alignment: .leading) {
                Color.white
                Text(dateString)
                    .font(.system(size: 15, weight: .regular))
                    .padding(.leading, 6)
            }
            .cornerRadius(5)
            .padding(5)
            .opacity(active ? 0 : 1)
            .allowsHitTesting(false)
        )
        .padding(3)
        .overlay(
            RoundedRectangle(cornerRadius: 7)
                .stroke(lineWidth: 2)
                .foregroundColor(.appBlue)
                .opacity(active ? 1 : 0)
        )
    }
    
    var dateString: String {
        date?.asTitleString ?? "YYYY-MM-DD"
    }
}
extension Optional {
    func defaulted(to value: Wrapped) -> Wrapped {
        return self ?? value
    }
}

struct CustomCalendarView: View {
    @Binding var start: Date?
    @Binding var end: Date?

    @State var seletionTitle = "Today"

    let calendarType: CalendarSelection
    let mainDates: [Date]
    let closeCalendarAction: () -> Void

    @Binding var selectionForMain: CalendarOptions
    @Binding var selectionForCompare: CompareOptions
    
    @State var startFieldText = ""
    @State var startFieldActive = false
    
    @State var endFieldText = ""
    @State var endFieldActive = false

    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            Menu {
                if calendarType == .main {
                    Picker(selection: $selectionForMain) {
                        ForEach(CalendarOptions.dataSource, id: \.self) { value in
                            Text("\(value.rawValue)")
                        }
                    } label: { }
                } else {
                    Picker(selection: $selectionForCompare) {
                        ForEach(CompareOptions.dataSource, id: \.self) { value in
                            Text("\(value.rawValue)")
                        }
                    } label: { }
                }
            } label: {
                BorderedButton(action: {}) {
                    HStack(spacing: 9) {
                        Text(seletionTitle)
                            .font(.system(size: 15, weight: .regular))
                            .foregroundColor(.black)
                        Spacer()
                        
                        Image("selectionArrows")
                            .resizable()
                            .frame(width: 5, height: 10)
                    }
                }
            }
            .onChange(of: selectionForMain, perform: performActionOnSelection)
            .onChange(of: selectionForCompare, perform: performActionOnCompareSelection)
                
            HStack {
                CustomTextField(text: $startFieldText, date: $start, selectionForMain: $selectionForMain, selectionForCompare: $selectionForCompare, calendarType: .main)

                Spacer()

                Image(systemName: "arrow.right")
                    .resizable()
                    .foregroundColor(.gray)
                    .frame(width: 13, height: 10)

                Spacer()
                
                CustomTextField(text: $endFieldText, date: $end, selectionForMain: $selectionForMain, selectionForCompare: $selectionForCompare, calendarType: .comparison)
            }
           
            
            DatePView(firstSelection: $start,
                      secondSelection: $end,
                      calendarType: calendarType,
                      selectionForMain: $selectionForMain,
                      selectionForCompare: $selectionForCompare)
                .padding(.horizontal, 6)
            
            HStack {
                Button(action: closeCalendarAction, label: {
                    Text("Cancel")
                        .font(.system(size: 13, weight: .regular))
                        .padding(.vertical, 8)
                        .padding(.horizontal, 16)
                        .overlay(
                            RoundedRectangle(cornerRadius: 2)
                                .stroke()
                                .foregroundColor(.gray.opacity(0.6))
                        )
                })
                .buttonStyle(.plain)
                Spacer()
                
                Button(action: closeCalendarAction, label: {
                    Text("Apply")
                        .font(.system(size: 13, weight: .regular))
                        .foregroundColor(.white)
                        .padding(.vertical, 8)
                        .padding(.horizontal, 16)
                        .background(Color.appGreen)
                        .cornerRadius(2)
                })
//                .disabled(start.isInToday)
                .buttonStyle(.plain)
            }
        }
        .padding(.vertical, 16)
        .padding(.horizontal, 18)
        .background(
            Color.white
                .shadow(color: Color.black.opacity(0.1), radius: 4)
        )
        .cornerRadius(4)
        .onAppear(perform: {
            self.startFieldText = start?.asString ?? "YYYY-MM-DD"
            self.endFieldText = end?.asString ?? "YYYY-MM-DD"
            self.performActionOnSelection(selectionForMain)
            
        })
        .onChange(of: start) { newValue in
            self.startFieldText = newValue?.asString ?? "YYYY-MM-DD"
        }
        .onChange(of: end) { newValue in
            self.endFieldText = newValue?.asString ?? "YYYY-MM-DD"
        }
        .onChange(of: calendarType) { newValue in
            self.seletionTitle = newValue == .main ? selectionForMain.rawValue : selectionForCompare.rawValue
            
            if newValue == .comparison {
                performActionOnCompareSelection(selectionForCompare)
            }
        }
    }
    
    func performActionOnCompareSelection(_ selection: CompareOptions) {
        self.seletionTitle = selection.rawValue

        let dates = getDatesOnCompare(selection)
        self.start = dates.date1
        self.end = dates.date2
    }

    func performActionOnSelection(_ selection: CalendarOptions) {
        self.seletionTitle = selection.rawValue
        
        guard selection != .custom else { return }

        let dates = getDatesOnSelection(selection)
        self.start = dates.date1
        self.end = dates.date2
    }
    
    func getDatesOnCompare(_ selection: CompareOptions) -> (date1: Date?, date2: Date? ) {
        switch selection {
        case .noComparison: return (nil, nil)
        case .previousPeriod:
            if let first = mainDates.first, mainDates.count == 1 {
                return (first.dayBefore, nil)
            } else if mainDates.count >= 2 {
                let dates = previousPeriod(date1: mainDates.first!, date2: mainDates.last!)
//                previousPeriod(startDate: mainDates.last!, previousDate: mainDates.first!)
                return (dates.0, dates.1)
            }
            return (nil, nil)
        case .firstQ:
            let days = Date().firstAndLastDatesForQuarter(1)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .secondQ:
            let days = Date().firstAndLastDatesForQuarter(2)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .thirdQ:
            let days = Date().firstAndLastDatesForQuarter(3)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .fourthQ:
            let days = Date().firstAndLastDatesForQuarter(4)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .BFCM19: return (Date().dateForBFCMYear(2019) ?? Date(), nil)
        case .BFCM20: return (Date().dateForBFCMYear(2020) ?? Date(), nil)
        case .BFCM21: return (Date().dateForBFCMYear(2021) ?? Date(), nil)
        case .BFCM22: return (Date().dateForBFCMYear(2022) ?? Date(), nil)
        case .custom: return (nil, nil)
        }
    }

    func getDatesOnSelection(_ selection: CalendarOptions) -> (date1: Date?, date2: Date? ) {
        switch selection {
        case .today: return (Date(), nil)
        case .yesterday: return (Date.yesterday, nil)
        case .last7days:
            let days = Date().firstAndLastDatesForLast(6)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .last30days:
            let days = Date().firstAndLastDatesForLast(30)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .last90days:
            let days = Date().firstAndLastDatesForLast(90)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .lastMonth: return (Date().startOfPreviousMonth(), Date().endOfPreviousMonth())
        case .lastYear:
            let days = Date().previousYear()
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .weekToDate: return (Date().weekToDate(), Date())
        case .monthToDate: return (Date().monthArray().first, Date())
        case .quarterToDate: return (Date().startOfQuarter(), Date())
        case .yearToDate: return (Date().startOfYear(), Date())
        case .firstQ:
            let days = Date().firstAndLastDatesForQuarter(1)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .secondQ:
            let days = Date().firstAndLastDatesForQuarter(2)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .thirdQ:
            let days = Date().firstAndLastDatesForQuarter(3)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .fourthQ:
            let days = Date().firstAndLastDatesForQuarter(4)
            return (days?.firstDate ?? Date(), days?.lastDate)
        case .BFCM19: return (Date().dateForBFCMYear(2019) ?? Date(), nil)
        case .BFCM20: return (Date().dateForBFCMYear(2020) ?? Date(), nil)
        case .BFCM21: return (Date().dateForBFCMYear(2021) ?? Date(), nil)
        case .BFCM22: return (Date().dateForBFCMYear(2022) ?? Date(), nil)
        case .custom: return (nil, nil)
        }
    }
    
    func previousPeriod(date1: Date, date2: Date) -> (Date, Date) {
        let length = Calendar.current.dateComponents([.day], from: date1, to: date2).day!

        let secondDate = Calendar.current.date(byAdding: .day, value: -1, to: date1)!
        let firstDate = Calendar.current.date(byAdding: .day, value: -length, to: secondDate)!
        return (firstDate, secondDate)
    }
}
