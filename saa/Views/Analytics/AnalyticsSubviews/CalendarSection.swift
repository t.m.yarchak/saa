//
//  CalendarSection.swift
//  saa
//
//

import SwiftUI

enum CalendarSelection {
    case main
    case comparison
}

struct CalendarSection: View {
    let mainDates: [Date]
    let datesToCompare: [Date]
    let calendarAction: (CalendarSelection) -> Void
    
    @State var calendarBSize: CGSize = .zero
    @State var compareBSize: CGSize = .zero
    
    @State var useVStack = false
    
    let selectionForMain: CalendarOptions
    let selectionForCompare: CompareOptions

    var body: some View {
        VStack(alignment: .leading, spacing: 25) {
            Text("Analytics")
                .font(.system(size: 20, weight: .semibold))
           
            VStack(alignment: .leading, spacing: 12) {
                HStack {
                    calendarButton
                        .readSize { size in
                            calendarBSize = size
                            
                            useVStack = size.width >= UIScreen.screenWidth / 2.5 && compareBSize.width >= UIScreen.screenWidth / 2.5
                        }
                    Spacer()
                    
                    if !useVStack {
                        compareButton
                            .readSize { size in
                                compareBSize = size
                                useVStack = size.width >= UIScreen.screenWidth / 2.5 && calendarBSize.width >= UIScreen.screenWidth / 2.5
                            }
                    }
                }
                
                if useVStack {
                    compareButton
                        .readSize { size in
                            compareBSize = size
                            useVStack = size.width >= UIScreen.screenWidth / 3 && calendarBSize.width >= UIScreen.screenWidth / 3
                        }
                }
            }
            
            
            autoRefresh
        }
        .padding(.horizontal, 15)
        .padding(.top, 24)
        .padding(.bottom, 8)
    }
    
    var calendarButton: some View {
        Button(action: { calendarAction(.main) }) {
            HStack(spacing: 9) {
                Image("calendarIcon")
                    .resizable()
                    .frame(width: 12.5, height: 14)
                
                Text(extractTitle(mainDates))
                    .font(.system(size: 13, weight: .medium))
                    .foregroundColor(.black)
                    .lineLimit(1)
            }
            .buttonStyle(.plain)
            .padding(.horizontal, 12)
            .padding(.vertical, 6)
            .background(Color.white)
            .overlay(
                RoundedRectangle(cornerRadius: 0)
                    .stroke()
                    .foregroundColor(.gray)
            )
        }
    }

    var compareButton: some View {
        Button(action: { calendarAction(.comparison) }) {
            HStack(spacing: 9) {
                Text(extractTitleToCompare(datesToCompare))
                    .font(.system(size: 13, weight: .medium))
                    .foregroundColor(.black)
                    .lineLimit(1)
            }
            .buttonStyle(.plain)
            .padding(.horizontal, 12)
            .padding(.vertical, 6)
            .background(Color.white)
            .overlay(
                RoundedRectangle(cornerRadius: 2)
                    .stroke()
                    .foregroundColor(.gray)
            )
        }
    }
    
    var autoRefresh: some View {
        HStack(spacing: 4) {
            RoundedRectangle(cornerRadius: 2)
                .stroke(Color.gray, lineWidth: 1.5)
                .background(Color.white)
                .frame(width: 16, height: 16)
                .padding(.trailing, 6)
            Text("Auto-refresh")
                .font(.system(size: 12, weight: .regular))
            
            Image("infoIcon")
                .resizable()
                .scaledToFit()
                .frame(width: 16, height: 16)
        }
    }

    func extractTitle(_ dates: [Date]) -> String {
        guard selectionForMain == .custom else {
            return selectionForMain.rawValue
        }
        
        guard !dates.isEmpty else { return "Today" }
        if let first = dates.first, dates.count == 1 {
            return first.isInToday ? "Today" : first.isInYesterfay ? "Yesterday" : first.asTitleString
        } else {
            guard let first = dates.first, let last = dates.last else {
                return "Today"
            }
            return first.asTitle(plusDate: last)
        }
    }

    func extractTitleToCompare(_ dates: [Date]) -> String {
        if selectionForCompare == .noComparison {
            return selectionForCompare.rawValue
        } else if selectionForCompare != .custom {
            return "Compare: " + selectionForCompare.rawValue
        } else {
            guard !dates.isEmpty else { return "No comparison" }
            let string = extractTitle(dates)
            return "Compare: " + string
        }
    }
}
