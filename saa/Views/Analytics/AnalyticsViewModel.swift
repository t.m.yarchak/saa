//
//  AnalyticsViewModel.swift
//  saa
//
//

import Combine
import Foundation

extension TimeZone {
    static var utc = TimeZone(abbreviation: "UTC")!
}

enum PlaceholderLoad: Int {
    case activity
    case firstPlaceholder
    case secondPlaceholder
    case loaded
}

final class AnalyticsViewModel: BaseViewModel {
    private unowned let coordinator: AppCoordinator

    @Published var placeholderLoad: PlaceholderLoad = .activity
    
    @Published var mainSaleDay: SaleDay?
    @Published var saleDayToCompare: SaleDay?
    
    @Published var mainSelectedDates = [Date]()
    @Published var selectedDatesToCompare: [Date] = []
    
    
    @Published var calendarStartDate: Date? = Date()
    @Published var calendarEndDate: Date?
    @Published var calendarType: CalendarSelection = .main
    
    @Published var selectionForMain: CalendarOptions = .today
    @Published var selectionForCompare: CompareOptions = .noComparison
    
    var firstLoadPerformed = false
    
    var minItemCost: Double {
        managersProvider.appManager.infoDAO.currentUser?.minItemCost ?? 89.99
    }
    
    
    init(coordinator: AppCoordinator, selection: Filters) {
        self.coordinator = coordinator

        super.init(managersProvider: coordinator.managersProvider)
        switch selection {
        case .live, .today:
            self.selectionForMain = .today
            self.calendarStartDate = Date()
            self.mainSelectedDates = [Date()]
        case .yesterday:
            self.selectionForMain = .yesterday
            self.calendarStartDate = Date.yesterday
            self.mainSelectedDates = [Date.yesterday]
        case .thisWeek:
            self.selectionForMain = .weekToDate
            self.calendarStartDate = Date().weekToDate()
            self.mainSelectedDates = Date.dates(from: Date().weekToDate(), to: Date())
        case .thisMonth:
            self.selectionForMain = .monthToDate
            self.calendarStartDate = Date().monthArray().first
            self.mainSelectedDates = Date().monthArray().filter({ !$0.isInFuture})
        }
    }
    
    override func addSubscribers() {
        super.addSubscribers()

        $mainSelectedDates
            .combineLatest(managersProvider.appManager.$datesPreset)
            .sink { [weak self] dates, array in
                guard let self = self else { return }

                if self.firstLoadPerformed {
                    self.placeholderLoad = .secondPlaceholder
                }

                DispatchQueue.global(qos: .background).async {
                    let result = self.findDay(dates, array: array)
                       DispatchQueue.main.async {
                           self.mainSaleDay = result

                           if self.firstLoadPerformed {
                               self.placeholderLoad = .loaded
                           }
                       }
                   }
            }
            .store(in: &cancellables)
        
        $selectedDatesToCompare
            .combineLatest(managersProvider.appManager.$datesPreset)
            .sink { [weak self] dates, array in
                guard let self = self else { return }

                if self.firstLoadPerformed {
                    self.placeholderLoad = .secondPlaceholder
                }

                DispatchQueue.global(qos: .background).async {
                    let result = self.findDay(dates, array: array)
                       DispatchQueue.main.async {
                           self.saleDayToCompare = result

                           if self.firstLoadPerformed {
                               self.placeholderLoad = .loaded
                           }
                       }
                   }
            }
            .store(in: &cancellables)
    }
    
    func prepareCalendar(_ type: CalendarSelection) {
        switch type {
        case .main:
            self.calendarType = .main
            self.calendarStartDate = mainSelectedDates.first ?? Date()
            self.calendarEndDate = mainSelectedDates.count == 1 ? nil : mainSelectedDates.last
        case .comparison:
            self.calendarType = .comparison
            self.calendarStartDate = selectedDatesToCompare.first ?? Date.yesterday
            self.calendarEndDate = selectedDatesToCompare.count == 1 ? nil :selectedDatesToCompare.last
        }
    }
    
    func closeCalendar() {
        guard let endDate = calendarEndDate else {
            switch self.calendarType {
            case .main:
                self.mainSelectedDates = [(calendarStartDate ?? Date())]
            case .comparison:
                if let calendarStartDate = calendarStartDate {
                    self.selectedDatesToCompare = [calendarStartDate]
                } else {
                    self.selectedDatesToCompare = []
                }
            }
            return
        }

        switch self.calendarType {
        case .main:
            self.mainSelectedDates = Date.dates(from: (calendarStartDate ?? Date()), to: endDate)
        case .comparison:
            if let calendarStartDate = calendarStartDate {
                self.selectedDatesToCompare = Date.dates(from: calendarStartDate, to: endDate)
            } else {
                self.selectedDatesToCompare = []
            }
        }
    }
    
    func findDay(_ dates: [Date], array: [SaleDay]) -> SaleDay? {
        guard !dates.isEmpty else { return nil }
        guard dates.count != 1 else { return self.computeMainSelection(dates, array: array) }
        
        return self.computeMainSelectionForRange(dates, array: array)
    }

    func computeMainSelection(_ dates: [Date], array: [SaleDay]) -> SaleDay? {
        guard let date = dates.first, dates.count == 1 else { return nil }
        

        let asigned = array.first(where: { $0.date.isSameDay(as: date) == true })
        return asigned
    }

    func computeMainSelectionForRange(_ dates: [Date], array: [SaleDay]) -> SaleDay? {
        guard let first = dates.first, let startIndex = array.firstIndex(where: { $0.date.isSameDay(as: first) == true })  else {
            let salesDays = dates.compactMap { date in
                array.first(where: { $0.date.isSameDay(as: date) == true })
            }
            return SaleDay(range: salesDays)
        }
        
        let filteredObjects = Array(array[startIndex...])
        let salesDays = dates.compactMap { date in
            filteredObjects.first(where: { $0.date.isSameDay(as: date) == true })
        }
        return SaleDay(range: salesDays)
    }

    func close() {
        self.coordinator.closeAnalytics()
    }
}
