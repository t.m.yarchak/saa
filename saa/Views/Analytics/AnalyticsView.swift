//
//  AnalyticsView.swift
//  saa
//
//

import SwiftUI
import Combine

struct AnalyticsView: View {
    @ObservedObject var viewModel: AnalyticsViewModel
    @State var showCalendar = false
    
    func calendarAction(_ type: CalendarSelection) {
        showCalendar.toggle()
        switch type {
        case .main:
            self.viewModel.prepareCalendar(.main)
        case .comparison:
            self.viewModel.prepareCalendar(.comparison)
        }
    }
    
    func closeCalendarAction() {
        self.showCalendar.toggle()
        self.viewModel.closeCalendar()
    }

    var body: some View {
        VStack(spacing: 0) {
            HeaderView(action: viewModel.close)
                ScrollView {
                    VStack(spacing: 16) {
                        CalendarSection(mainDates: viewModel.mainSelectedDates, datesToCompare: viewModel.selectedDatesToCompare, calendarAction: calendarAction,selectionForMain: viewModel.selectionForMain, selectionForCompare: viewModel.selectionForCompare)

                        ChartSection(placeholderLoad: $viewModel.placeholderLoad, type: .sales, saleDay: viewModel.mainSaleDay, saleDayToCompare: viewModel.saleDayToCompare, selection: viewModel.selectionForMain, minItemCost: viewModel.minItemCost, mainSelectedDates: viewModel.mainSelectedDates)
            
                        RateSection(placeholderLoad: $viewModel.placeholderLoad, saleDay: viewModel.mainSaleDay, toCompare: viewModel.saleDayToCompare)

                        ChartSection(placeholderLoad: $viewModel.placeholderLoad, type: .orders, saleDay: viewModel.mainSaleDay, saleDayToCompare: viewModel.saleDayToCompare, selection: viewModel.selectionForMain, minItemCost: viewModel.minItemCost, mainSelectedDates: viewModel.mainSelectedDates)
                    }
                    .overlay(
                        VStack {
                            CustomCalendarView(start: $viewModel.calendarStartDate,
                                               end: $viewModel.calendarEndDate,
                                               calendarType: viewModel.calendarType,
                                               mainDates: viewModel.mainSelectedDates,
                                               closeCalendarAction: closeCalendarAction,
                                               selectionForMain: $viewModel.selectionForMain,
                                               selectionForCompare: $viewModel.selectionForCompare)
                                .padding(.horizontal, 16)
                                .shadow(radius: 1)
                                .offset(y: 110)
                            Spacer()
                        }
                        .opacity(showCalendar ? 1 : 0)
                    )
                }
                Spacer()
        }
        .ignoresSafeArea(.all, edges: .bottom)
        .background(Color.dividerColor.ignoresSafeArea())
        .overlay(PlainProgressView().opacity(viewModel.placeholderLoad == .activity ? 1 : 0))
        .onAppear {
            guard viewModel.placeholderLoad == .activity else { return }

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                viewModel.placeholderLoad = .firstPlaceholder
            })

            DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                viewModel.placeholderLoad = .secondPlaceholder
            })

            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                viewModel.placeholderLoad = .loaded
                self.viewModel.firstLoadPerformed = true
            })
        }
    }
}

enum RateItemSectionType: Int, CaseIterable {
    case cartAdded
    case checkoutReached
    case convertedSessions
    
    var title: String {
        switch self {
        case .cartAdded: return "Added to cart"
        case .checkoutReached: return "Reached checkout"
        case .convertedSessions: return "Sessions converted"
        }
    }
}

enum ChartSectionType: Int {
    case sales
    case orders
    
    var title: String {
        switch self {
        case .sales: return "Total sales"
        case .orders: return "Total orders"
        }
    }
    
    var subtitle: String {
        switch self {
        case .sales: return "Sales over time"
        case .orders: return "Orders over time"
        }
    }
}

struct AnalyticsView_Previews: PreviewProvider {
    static var previews: some View {
        AnalyticsView(viewModel: .init(coordinator: dev.appCoordinator, selection: .yesterday))
        //AnalyticsView(viewModel: .init(coordinator: dev.appCoordinator, selection: .yesterday))
    }
}

struct OneDaySale: Hashable {
    let amount: Double
    let date: Date
}

import Charts
struct LineChartView: View {
    let chartType: ChartSectionType
    let saleDay: SaleDay
    let selection: CalendarOptions
    let minItemCost: Double

    @State var chartData: [LinearChartData]
    
    var signString: String {
        chartType == .sales ? "$" : ""
    }
    
    var valueForXMark: AxisMarkValues {
        switch selection {
        case .today, .yesterday:
            return .stride(by: .hour, count: 6)
        case .monthToDate:
            return .stride(by: .day, count: 7)
        default:
            return saleDay.strideBy == 0 ? .automatic : .stride(by: .day, count: saleDay.strideBy)
        }
    }
    
    func maxValue() -> Int {
        var maxValue = chartData.map({ $0.amount }).max() ?? 0
        maxValue = chartType == .sales ? Int(Double(maxValue) * minItemCost) : maxValue
        return maxValue
    }
    
    func textForValue(_ value: Date) -> String {
        let dateFormatter = DateFormatter()
        
        if saleDay.type == .day || saleDay.strideBy == 0 {
            dateFormatter.dateFormat = "h:mm a"
            return dateFormatter.string(from: value)
        }
        dateFormatter.dateFormat = "MMM d"
        return dateFormatter.string(from: value)
    }

    
    var body: some View {
        Chart {
            ForEach(chartData) {
                LineMark(
                    x: .value("day", $0.date),
                    y: .value("amount", $0.animate ? (chartType == .sales ? Int(Double($0.amount) * minItemCost) : $0.amount) : 0)
                )
                .interpolationMethod(.monotone)
                
            }
            
        }
        .foregroundStyle(LinearGradient(gradient: Gradient(colors: [.blue, .appPurple]), startPoint: .top, endPoint: .bottom)
        )
        .padding(.horizontal, 15)
        .chartYScale(domain: 0...(maxValue()))
        .chartXAxis {
            AxisMarks(preset: .aligned, values: valueForXMark) { value in
                if let date = value.as(Date.self) {
                    AxisValueLabel() {
                        Text(textForValue(date))
                            .padding(.top, 10)
                    }
                }
            }
 
        }
        .chartYAxis {
            AxisMarks(preset: .automatic, position: .leading, values: .automatic) { value in
                AxisValueLabel() {
                    ZStack {
                        Color.dividerColor
                            .frame(height: 0.4)
                        
                        if let value = value.as(Int.self) {
                            Text(signString + "\(value.KFormatted)")
                                .background(Color.white)
                        }
                    }
                    .frame(width: 50)
                    .offset(x: 4)
                }
                AxisGridLine()
                    .foregroundStyle(Color.dividerColor)
            }
        }
        .onChange(of: saleDay, perform: { newValue in
            self.chartData = newValue.chartData
            self.animateGraph()
        })
        .onAppear{
            self.animateGraph()
        }
    }

    func animateGraph(fromChange: Bool = false){
        for (index,_) in chartData.enumerated(){
            DispatchQueue.main.asyncAfter(deadline: .now() + Double(index) * 0.02){
                withAnimation(.easeInOut(duration: 0.6)){
                    chartData[index].animate = true
                }
            }
        }
    }
}

struct EmptyChartView: View {
    let dates: [Date]

    var body: some View {
        Chart {
            ForEach(dates, id: \.self) { date in
                LineMark(x: .value("period", date), y: .value("amount", 0.1))
                    .interpolationMethod(.monotone)
            }
        }
        .foregroundStyle(LinearGradient(gradient: Gradient(colors: [.blue, .appPurple]), startPoint: .top, endPoint: .bottom))
        .padding(.horizontal, 15)
        .chartXAxis {
            AxisMarks(preset: .aligned, values: .automatic) { value in
                AxisValueLabel()
                    .offset(y: 8)
            }
        }
        .chartYAxis {
            AxisMarks(preset: .automatic, position: .leading, values: [0, 5, 10]) { value in
                AxisValueLabel()
                AxisGridLine(centered: true)
            }
        }
    }
}

import Foundation

struct SizePreferenceKey: PreferenceKey {
  static var defaultValue: CGSize = .zero
  static func reduce(value: inout CGSize, nextValue: () -> CGSize) {}
}

