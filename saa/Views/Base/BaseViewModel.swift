//
//  BaseViewModel.swift
//  saa
//
//

import Combine
import SwiftUI

class BaseViewModel: ObservableObject, Identifiable {
    var cancellables = Set<AnyCancellable>()

    let managersProvider: ManagersProvider

    init(managersProvider: ManagersProvider) {
        self.managersProvider = managersProvider
        self.addSubscribers()
    }

    func addSubscribers() { }
}
