//
//  HomeView.swift
//  saa
//
//

import Charts
import SwiftUI

enum HomeSections: Int, CaseIterable {
    case searchAndChart
    case chargebacks
    case billingCycle
    case customContent
}

struct HomeView: View {
    @ObservedObject var viewModel: HomeViewModel
    @State var text = ""
    @State var selectedFilter = Filters.today
    @State var isAnimating = false

    var body: some View {
        RefreshableScrollView {
            VStack(spacing: 0) {
                SearchAndChartSection(activeFilter: $viewModel.activeFilter,
                                      searchText: $text,
                                      chartImage: $viewModel.chartImage,
                                      placeholderLoad: viewModel.placeholderLoad,
                                      saleDay: viewModel.saleDay,
                                      chartBarData: viewModel.chartBarData,
                                      load: viewModel.placeholderLoad,
                                      onSelect: viewModel.onFilterSelect,
                                      action: viewModel.openAnalytics)
                .environmentObject(viewModel.carouselStateModel)

                ChargeBackSection(showChargebacks: viewModel.showChargebacks,
                                  showOrders: viewModel.showOrders,
                                  chargebacks: viewModel.chargebacks,
                                  orders: viewModel.orders)

                if viewModel.placeholderLoad != .activity {
                    if viewModel.showOrders {
                        AbandonedCheckouts(leftBehind: viewModel.leftBehind)
                    }
                    BillingSection()
                    BuildContentSection()
                }
            }
            .background(Color.white)
            
            if viewModel.placeholderLoad != .loaded {
                ProgressView()
                    .scaleEffect(1.4)
                    .offset(y: UIScreen.screenHeight / 18)
                    .opacity(viewModel.placeholderLoad == .activity ? 1 : 0)
            }
        }
        .onAppear(perform: onAppear)
        .background(Color.dividerColor.offset(y: UIScreen.screenHeight / 2.5))
        .refreshable {
            viewModel.updateChart()
        }
    }

    func onAppear() {
        guard viewModel.placeholderLoad == .activity else { return }

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: { withAnimation {
            viewModel.placeholderLoad = .loaded
        }})
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(viewModel: HomeViewModel(coordinator: dev.appCoordinator))
    }
}
