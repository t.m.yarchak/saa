//
//  BillingSection.swift
//  saa
//
//

import SwiftUI

struct BillingSection: View {
    var body: some View {
        VStack(spacing: 0) {
            VStack(alignment: .leading, spacing: 10) {
                HStack {
                    Text("Give your business time to grow")
                        .font(.system(size: 16, weight: .medium))
                    
                    Spacer()
                    
                    Image("moreDots")
                        .resizable()
                        .frame(width: 20, height: 20)
                }
                
                Text("Switch to a Shopify yearly plan and save up to 25%")
                    .font(.system(size: 14, weight: .regular))
                
                Text("Change billing cycle")
                    .foregroundColor(.appBlue)
                    .font(.system(size: 16, weight: .regular))
                    .padding(.top, 4)
            }
            .padding(15)
            
            Color.dividerColor
                .frame(height: 8)
        }
    }
}
