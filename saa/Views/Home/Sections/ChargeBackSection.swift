//
//  ChargeBackSection.swift
//  saa
//

import SwiftUI

struct ChargeBackSection: View {
    let showChargebacks: Bool
    let showOrders: Bool
    let chargebacks: Int
    let orders: Int

    var body: some View {
        VStack(spacing: 0) {
            if showOrders {
                HStack(spacing: 16) {
                    Image("orderIcon")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 14.5, height: 18)

                    HStack(spacing: 5) {
                        Text("\(orders) orders")
                            .font(.system(size: 16, weight: .semibold))
                        Text("to fulfill")
                            .font(.system(size: 16, weight: .regular))
                    }

                    Spacer()
                    
                    Image("openArrow")
                        .resizable()
                        .frame(width: 6, height: 11)
                }
                .padding(.horizontal, 20)
                .padding(.vertical, 12)
                
                Divider()
                    .padding(.horizontal, 20)
            }
            
            if showChargebacks {
                HStack(spacing: 16) {
                    Image("chargeBack")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 14.5, height: 18)
                    
                    HStack(spacing: 5) {
                        Text("\(chargebacks) chargebacks")
                            .font(.system(size: 16, weight: .semibold))
                        Text("to review")
                            .font(.system(size: 16, weight: .regular))
                    }
                    
                    Spacer()
                    
                    Image("openArrow")
                        .resizable()
                        .frame(width: 6, height: 11)
                }
                .padding(.horizontal, 20)
                .padding(.vertical, 12)
            }
            
            Color.dividerColor
                .frame(height: 8)
        }
    }
}
