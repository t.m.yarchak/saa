//
//  ChartView.swift
//  saa
//

import SwiftUI
import Charts

struct ChartView: View {
    let chartImage: ChartImageEnum
    let placeholderLoad: PlaceholderLoad
    let saleDay: SaleDay?
    let minItemCost: Double
    
    var body: some View {
        VStack(spacing: 20) {
            HStack {
                VStack(alignment: .trailing, spacing: 3) {
                    Text("Visitors")
                        .font(.system(size: 14, weight: .regular))
                    
                    Text("-")
                        .font(.system(size: 24, weight: .semibold))
                    Text("-")
                        .font(.system(size: 12, weight: .regular))
                }
                .offset(x: -20)
                .foregroundColor(.secondary)

                Spacer()

                VStack(spacing: 3) {
                    Text("Total sales")
                        .font(.system(size: 14, weight: .regular))
                        .foregroundColor(.secondary)
                    Text(placeholderLoad == .activity ? "-" : "$\(saleDay?.totalSales(minItemCost).stripeZeros ?? "200")")
                        .font(.system(size: 24, weight: .semibold))
                    Text(placeholderLoad == .activity ? "-" : "\(saleDay?.totalOrders ?? 5) orders")
                        .font(.system(size: 12, weight: .regular))
                }

                Spacer()

                VStack(alignment: .leading, spacing: 3) {
                    Text("Sessions")
                        .font(.system(size: 14, weight: .regular))
                        .foregroundColor(.secondary)
                    Text(placeholderLoad == .activity ? "-" : "\(saleDay?.sessions ?? 3)")
                        .font(.system(size: 24, weight: .semibold))
                    Text(placeholderLoad == .activity ? "-" : "\(saleDay?.visitors ?? 5) visitors")
                        .font(.system(size: 12, weight: .regular))
                }
                .foregroundColor(.secondary)
                .offset(x: 25)
                
            }

            Image(chartImage.imageName)
                .resizable()
                .scaledToFit()
                .padding(.horizontal)
        }
    }
}
