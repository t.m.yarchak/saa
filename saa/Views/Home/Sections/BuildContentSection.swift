//
//  BuildContentSection.swift
//  saa
//

import SwiftUI

struct BuildContentSection: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            
            Text("Build custom content or data structure for your store")
                .font(.system(size: 16, weight: .medium))
                .lineLimit(2)
            Text("Switch to a Shopify yearly plan and save up to 25%")
                .font(.system(size: 14, weight: .regular))
            
            Text("Add metaobject definition")
                .foregroundColor(.appBlue)
                .font(.system(size: 16, weight: .regular))
                .padding(.top, 4)
            
            HStack(spacing: 0) {
                Text("Learn more")
                    .foregroundColor(.appBlue)
                    .font(.system(size: 16, weight: .regular))
                    .padding(.top, 6)
                Spacer()
            }
        }
        .padding(15)
    }
}
