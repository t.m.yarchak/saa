//
//  ChartScrollView.swift
//  saa
//
//

import SwiftUI
import Charts
import UIKit

public class CarouselStateModel: ObservableObject {
    @Published var activeCard: Int = 1
    @Published var screenDrag: Float = 0.0
}

struct CarouselView: View {
    let selection: Filters
    let chartBarData: [ChartBarData]
    let numberOfItems: CGFloat
    let spacing: CGFloat
    let widthOfHiddenCards: CGFloat
    let totalSpacing: CGFloat
    let cardWidth: CGFloat
    let buttonAction: () -> Void

    @State var offset: Float = 0

    @GestureState var detectedDragGesture = false
    @EnvironmentObject var carouselStateModel: CarouselStateModel

    @inlinable public init(
        selection: Filters,
        chartBarData: [ChartBarData],
        spacing: CGFloat,
        widthOfHiddenCards: CGFloat, buttonAction: @escaping () -> Void) {
            self.selection = selection
            self.chartBarData = chartBarData
            self.numberOfItems = CGFloat(chartBarData.count)
            self.spacing = spacing
            self.buttonAction = buttonAction
            self.widthOfHiddenCards = widthOfHiddenCards
            self.totalSpacing = (CGFloat(numberOfItems) - 1) * spacing
            self.cardWidth = UIScreen.main.bounds.width - (widthOfHiddenCards * 2 ) - (spacing * 2)
        }

    var body: some View {
        VStack {
            carouselView
                .overlay {
                    VStack {
                        scrollView
                            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 100, alignment: .center)
                        Spacer()
                    }
                }
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
        }
    }

    var scrollView: some View {
        let totalCanvasWidth: CGFloat = (cardWidth / 2.1 * numberOfItems) + totalSpacing
        let xOffsetToShift = (totalCanvasWidth - UIScreen.main.bounds.width / 2.1) / 2
        let leftPadding = widthOfHiddenCards + spacing
        let totalMovement = cardWidth / 2.1 + spacing

        let activeOffset = xOffsetToShift + (leftPadding) - (totalMovement * CGFloat(carouselStateModel.activeCard))
        let nextOffset = xOffsetToShift + (leftPadding) - (totalMovement * CGFloat(carouselStateModel.activeCard) + 1)

        var calcOffset = Float(activeOffset)

        if (calcOffset != Float(nextOffset)) {
            calcOffset = Float(activeOffset) + carouselStateModel.screenDrag
        }
        
        return HStack(alignment: .center, spacing: spacing) {
            //toptems
            ForEach(chartBarData, id: \.self) {
                TopItem(barData: $0, allDataSource: chartBarData)
                  
            }
        }
        .offset(x: CGFloat(calcOffset), y: 0)
    }

    var carouselView: some View {
        let totalCanvasWidth: CGFloat = (cardWidth * numberOfItems) + totalSpacing
        let xOffsetToShift = (totalCanvasWidth - UIScreen.main.bounds.width) / 2
        let leftPadding = widthOfHiddenCards + spacing
        let totalMovement = cardWidth + spacing

        let activeOffset = xOffsetToShift + (leftPadding) - (totalMovement * CGFloat(carouselStateModel.activeCard))
        let nextOffset = xOffsetToShift + (leftPadding) - (totalMovement * CGFloat(carouselStateModel.activeCard) + 1)

        var calcOffset = Float(activeOffset)

        if (calcOffset != Float(nextOffset)) {
            calcOffset = Float(activeOffset) + carouselStateModel.screenDrag
        }

        return HStack(alignment: .center, spacing: spacing) {
            ForEach(chartBarData, id: \.self) { dataValue in
                VStack {
                    BottomItem(model: dataValue) {
                        MyViewControllerWrapper(data: dataValue)
                    }
                    
                    DashboardButton(selection: selection, action: buttonAction)
                        .frame(width: UIScreen.screenWidth - 35,height: 25)
                        .padding(.bottom, 15)
                        .padding(.top, 15)
                }
            }
        }
        .offset(x: CGFloat(calcOffset), y: 0)
        .onChange(of: calcOffset, perform: { newValue in
            self.offset = newValue
        })
        .gesture(
            DragGesture()
                .updating($detectedDragGesture) { currentState, gestureState, transaction in
                    self.carouselStateModel.screenDrag = Float(currentState.translation.width)
                }
                .onEnded { value in
                    self.carouselStateModel.screenDrag = 0
                    
                    if (value.translation.width < -50) &&  self.carouselStateModel.activeCard < Int(numberOfItems) - 1 {
                        self.carouselStateModel.activeCard += 1
                    }
                    
                    if (value.translation.width > 50) && self.carouselStateModel.activeCard > 0 {
                        self.carouselStateModel.activeCard -= 1
                    }
                }
        )
    }
}

struct BottomItem<Content: View>: View {
    @EnvironmentObject var carouselStateModel: CarouselStateModel

    var model: ChartBarData
    var content: Content
    @State var animation: Animation? = nil

    @inlinable public init(model: ChartBarData, @ViewBuilder _ content: () -> Content) {
        self.model = model
        self.content = content()
    }

    var body: some View {
        content
            .frame(width: UIScreen.main.bounds.width, height: 200)
    }
}

struct TopItem: View {
    @EnvironmentObject var carouselStateModel: CarouselStateModel

    let barData: ChartBarData
    let dataSource: [ChartBarData]
    @State var animation: Animation? = nil

    @inlinable public init(barData: ChartBarData, allDataSource: [ChartBarData]) {
        self.barData = barData
        self.dataSource = allDataSource
    }

    var body: some View {
        ZStack {
            Color.clear
            VStack(alignment: alignmentForView(item: barData) ,spacing: 2) {
                Text(barData.type.mainTitle)
                    .font(.system(size: 14, weight: .regular))
                    .foregroundColor(.secondary)
                Text(barData.isPlaceholder ? "-" : barData.type.mainValue)
                    .font(.system(size: 24, weight: .semibold))
                    .foregroundColor(.black)
                Text(barData.isPlaceholder ? "-" : barData.type.secondaryTitle)
                    .font(.system(size: 12, weight: .regular))
                    .foregroundColor(.secondary)
            }
            .opacity(opacityForView(item: barData))
        }
        .allowsHitTesting(false)
        .frame(width: UIScreen.main.bounds.width / 2.1)
    }
    
    func opacityForView(item: ChartBarData) -> Double {
        let indexForValue = dataSource.firstIndex(of: item) ?? 0
        if carouselStateModel.activeCard == indexForValue {
            return 1
        } else {
            return 0.5
        }
    }
    
    func alignmentForView(item: ChartBarData) -> HorizontalAlignment {
        let indexForValue = dataSource.firstIndex(of: item) ?? 0
        if carouselStateModel.activeCard == indexForValue {
            return .center
        } else if carouselStateModel.activeCard < indexForValue {
            return .leading
        } else {
            return .trailing
        }
    }
}

struct MyViewControllerWrapper: UIViewControllerRepresentable {
     let data: ChartBarData

    func makeUIViewController(context: Context) -> ChartBarController {
        return ChartBarController()
    }

    func updateUIViewController(_ uiViewController: ChartBarController, context: Context) {
        uiViewController.dataSource.send(data)
    }
}

import Combine

class ChartBarController: UIViewController {
    var dataSource = CurrentValueSubject<ChartBarData?, Never>(nil)
    var cancellables = Set<AnyCancellable>()

    lazy var vc = UIHostingController(rootView: BarChartExampleView(data: dataSource.value))

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addChild(vc)
        self.view.addSubview(vc.view)
    
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: self.view.centerYAnchor),
            vc.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])

        dataSource
            .sink { [weak self] dataSource in
                self?.vc.rootView.data = dataSource
            }
            .store(in: &cancellables)
    }
}



struct BarChartExampleView: View {
    var data: ChartBarData?
    
    var dataSource: [BarDataInfo] {
        data?.barInfo ?? []
    }
    
    var isPlaceholder: Bool {
        data?.isPlaceholder == true || dataSource.isEmpty || dataSource.map({ $0.value }).reduce(0, +) == 0
    }
    
    var isLivePlaceholder: Bool {
        if case .live(_) = data?.type {
            return true
        }
        return false
    }
    
    func valuesForX() -> [Date] {
        return dataSource.map({ $0.date })
    }
    
    
    func textForValue(_ value: Date) -> String {
        let dateFormatter = DateFormatter()
        
        
        dateFormatter.dateFormat = data?.type.dateFormat ?? "d"
        return dateFormatter.string(from: value)
    }
    
    var valueForXMark: AxisMarkValues {
        switch data?.type {
        case .weekSales, .weekSessions:
            return .stride(by: .day)
        case .todaySales, .todaySessions, .yesterdaySales, .yesterdaySessions:
            return .stride(by: .hour, count: 4)
        case .monthSales, .monthSessions:
            return .stride(by: .day, count: 5)
        default: return .automatic
        }
    }
    
    func colorForBar(_ date: Date) -> Color {
        return date.isiInFutureTime ? Color.secondary.opacity(0.5) : Color.barColor
    }
    
    var body: some View {
        Chart {
            ForEach(dataSource, id: \.self) { source in
                BarMark(x: .value("day", source.date), y: .value("amount", isPlaceholder ? -1 : source.value))
                    .foregroundStyle(colorForBar(source.date))
                    .cornerRadius(0)
            }
        }
        .padding(.horizontal, 15)
        .overlay{
            if let data = data, data.isPlaceholder, case .todaySales =  data.type  {
                ZStack {
                    ProgressView()
                        .offset(y: -14)
                }
            }
        }
        .chartXAxis {
            if isLivePlaceholder {
                AxisMarks(values: [dataSource[3].date]) {
                    AxisValueLabel {
                        Text("Real time pageviews in the last 10 minutes")
                    }
                }
            } else if !isPlaceholder {
                AxisMarks(preset: .aligned, values: valueForXMark) { value in
                    if let date = value.as(Date.self) {
                        AxisValueLabel() {
                            Text(textForValue(date))
                        }
                    }
                    AxisGridLine(centered: true)
                }
            } else {
                AxisMarks(values: .automatic) { value in
                    if let date = value.as(Date.self) {
                        AxisValueLabel() {
                            Text("")
                        }
                    }
                }
            }
        }
        .chartYAxis {
            if isPlaceholder || isLivePlaceholder {
                AxisMarks(preset: .aligned, position: .leading, values: [0, 300, 600, 900]) { value in
                    AxisGridLine(centered: true)
                    AxisTick(length: 12)
                    AxisValueLabel() {
                        if let value = value.as(Int.self) {
                            Text("\(value / 100)")
                        }
                    }
                }
            } else {
                AxisMarks(preset: .aligned, position: .leading, values: .automatic) { value in
                    AxisGridLine(centered: true)
                    AxisTick(length: 12)
                    AxisValueLabel() {
                        HStack {
                            Spacer()
                            if let value = value.as(Int.self) {
                                Text("\(value.KFormatted)")
                            }
                        }
                    }
                }
            }
        }
    }
}
