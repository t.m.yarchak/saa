//
//  HomeViewModel.swift
//  saa
//
//


import Combine
import Foundation

final class HomeViewModel: BaseViewModel {
    private unowned let coordinator: AppCoordinator
    @Published var animating = true
    @Published var placeholderLoad: PlaceholderLoad = .activity
    @Published var chartImage: ChartImageEnum = .chart1
    @Published var chargebacks = 2
    @Published var orders = 2
    @Published var showChargebacks = true
    @Published var showOrders = false
    @Published var leftBehind: String = "1979"
    @Published var saleDay: SaleDay?
    @Published var chartBarData: [ChartBarData] = GeneralConstants.placeholderBarData
    
    @Published var activeFilter: Filters = .today

    private(set) var carouselStateModel: CarouselStateModel = CarouselStateModel()
    
    init(coordinator: AppCoordinator) {
        self.coordinator = coordinator

        super.init(managersProvider: coordinator.managersProvider)
    }
    
    override func addSubscribers() {
        super.addSubscribers()
        
        carouselStateModel.$activeCard
            .sink { [weak self] index in
                guard let filter = FilterChartSection(rawValue: index) else { return }

                switch filter {
                case .live:
                    self?.activeFilter = .live
                case .todaySessions, .todaySales:
                    self?.activeFilter = .today
                case .yesterdaySessions, .yesterdaySales:
                    self?.activeFilter = .yesterday
                case .weekSessions, .weekSales:
                    self?.activeFilter = .thisWeek
                case .monthSessions, .monthSales:
                    self?.activeFilter = .thisMonth
                }
            }
            .store(in: &cancellables)

        managersProvider.appManager.$datesPreset
            .sink { [weak self] array in
                self?.saleDay = array.first(where: { $0.date.isSameDay(as: Date()) == true })
            }
            .store(in: &cancellables)
        
        managersProvider.appManager.$chartBarData
            .filter({ !$0.isEmpty })
            .debounce(for: .seconds(1), scheduler: RunLoop.main)
            .sink { [weak self] barData in
                self?.chartBarData = barData
            }
            .store(in: &cancellables)

        managersProvider.appManager.$user
            .compactMap({ $0 })
            .sink(receiveValue: { [weak self] user in
                self?.showChargebacks = user.showChargebacks
                self?.showOrders = user.showOrders
                self?.orders = user.orders
                self?.chargebacks = user.chargebacks
                self?.leftBehind = user.leftBehind
            })
            .store(in: &cancellables)
    }

    func openAnalytics() {
        self.coordinator.openAnalytics(selection: self.activeFilter)
    }

    func updateChart() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.chartImage = self.chartImage.updateByOne()
        })
    }
    
    func onFilterSelect(_ filter: Filters) {
        self.carouselStateModel.activeCard = filter.rawValue
        
    }

    private func generateMonthlyDates() {
        let date = Date()
        let alldatesArray = date.getAllDates()
        
        let salesdayArray = alldatesArray.map { date -> SaleDay in
           return SaleDay(date: date)
        }
        
        guard !salesdayArray.isEmpty else { return }
        self.managersProvider.appManager.updateDatesPreset(dates: salesdayArray)
    }
}
